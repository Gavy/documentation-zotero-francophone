---
hide:
  - navigation
  - toc
---

# Accueil

![logo de Zotero](./images/citeproc.svg){id=imghome}

Cette documentation a pour but de vous permettre de découvrir le logiciel Zotero et d'approfondir vos connaissances pour l'utiliser plus efficacement.


<div class="grid cards" markdown>

-   :simple-zotero:{ .lg .middle } __Guide rapide__

    ---

    Survoler les principales fonctionnalités de Zotero pour bien débuter.

    [:octicons-arrow-right-24: Démarrez avec Zotero](./quick_start_guide.md)

-   :material-download:{ .lg .middle } __Installation__

    ---

    Installer Zotero sur Windows, Mac et Linux

    [:octicons-arrow-right-24: Installez l’application Zotero et le connecteur pour votre navigateur](./installation.md)

-   :material-file-document-multiple:{ .lg .middle } __Toute la documentation__

    ---

    Accéder à la documentation complète de Zotero : collecte, organisation, citation, groupes, synchronisation, lecteur de PDF, prise de notes, etc.

    [:octicons-arrow-right-24: Parcourez la documentation en français](./adding_items_to_zotero.md)

-   :fontawesome-solid-circle-question:{ .lg .middle } __Base de connaissance__

    ---

    Trouver les réponses aux questions les plus fréquentes concernant Zotero.

    [:octicons-arrow-right-24: Explorez la base de connaissance](./kb/index.md)

</div>

!!! info "Documentation francophone Zotero"
    Il s'agit d'une traduction bénévole de la documentation officielle disponible en anglais sur [zotero.org](https://www.zotero.org/support/), avec l'autorisation de la [Corporation for Digital Scholarship](https://digitalscholar.org/) pour l'utilisation du nom et du logo Zotero. Cette traduction s'efforce de rester au plus proche des formulations originales : dans les pages traduites, "nous" renvoie ainsi à l'auteur original, c'est-à-dire l'équipe de développement de Zotero.

    Si vous souhaitez contribuer à la documentation, vous pouvez écrire à l'adresse [contact@zotero-fr.org](mailto:contact@zotero-fr.org) et/ou consulter les instructions du [dépôt Framagit][repo].

!!! info "Documentation « Développeur »"

    La documentation à destination des développeur·ses et personnes ayant un usage avancé souhaitant par exemple créer ou modifier des styles n'a pas été traduite ici. Vous pouvez consulter la [documentation officielle en anglais de Zotero](https://www.zotero.org/support/dev/start).

[repo]: https://framagit.org/zotero-fr/documentation-zotero-francophone
