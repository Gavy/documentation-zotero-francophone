# Sécurité de Zotero

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Zotero Security](https://www.zotero.org/support/security) - dernière mise à jour de la traduction : 2023-06-01*
</div>

!!! warning "Contacter l’équipe de sécurité"

    Si vous pensez avoir découvert une faille de sécurité dans Zotero, merci de nous contacter à l'adresse suivante : [security@zotero.org](mailto:security@zotero.org).

Zotero a été créé avec pour philosophie que vos données de recherche vous appartiennent et qu’elles doivent rester sécurisées et privées par défaut.

L’intégralité du logiciel Zotero est [open source](https://github.com/zotero) et peut être audité en termes de sécurité et de confidentialité. Les builds Zotero pour macOS et Windows sont signés et tous les builds sont distribués en utilisant le chiffrement lors du transport des données, garantissant que la version que vous utilisez est la dernière version que nous avons publiée.

Contrairement à de nombreux outils utilisables dans le cloud, l’application de bureau Zotero est un programme local qui s’exécute sur votre ordinateur et qui par défaut enregistre localement toutes les données de recherche. À moins que vous activiez explicitement la synchronisation, vos données de recherche ne quittent jamais votre ordinateur.

Si les politiques institutionnelles vous empêchent de mettre en ligne des données sur des serveurs tiers, Zotero peut toujours être utilisé localement sans synchronisation des données, mais la synchronisation est nécessaire pour utiliser les fonctionnalités de groupe.

Si vous choisissez de synchroniser vos données avec les serveurs Zotero, toutes les données en transit sont chiffrées selon les meilleures pratiques actuelles ([le endpoint (point de terminaison) de l’API de Zotero obtient le score A+](https://www.ssllabs.com/ssltest/analyze.html?d=api.zotero.org&hideResults=on) au test reconnu de SSL Labs) et stockées dans le cloud d’Amazon, dont l’accès est strictement limité au nombre restreint de membres de Zotero qui ont besoin de pouvoir y accéder pour maintenir le service. Les données des nouveaux comptes sont aussi chiffrées sur les serveurs de Zotero en utilisant l’algorithme de chiffrement AES-256. Toutes les données sont actuellement stockées dans la région us-east 1 d’Amazon Web Services (AWS), aux Etats-Unis (USA).

Bien que les données des bibliothèques et les pièces jointes des groupes puissent être synchronisées uniquement avec les serveurs de Zotero, il est possible de choisir de synchroniser les pièces jointes des bibliothèques personnelles soit avec les serveurs de Zotero, soit avec un serveur webDAV que vous contrôlez. Vous pouvez également utiliser des fichiers liés qui sont stockés à l’emplacement de votre choix et ne sont pas synchronisés par Zotero.

Le serveur de données de Zotero est open source et peut être exécuté localement, ce que certaines organisations choisissent de faire, mais cela peut être difficile techniquement, et nous n’assurons actuellement aucun support pour ces installations.

Consultez notre [politique de confidentialité](./privacy.md) pour plus de détails sur la collecte de données de Zotero et sur l’utilisation des données que vous choisissez de partager.