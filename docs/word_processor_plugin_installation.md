# Installer les extensions de traitement de texte de Zotero

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Installing the Zotero Word Processor Plugins](https://www.zotero.org/support/word_processor_plugin_installation) - dernière mise à jour de la traduction : 2025-01-12*
</div>

Les [extensions de traitement de texte](./word_processor_integration.md) sont fournies avec Zotero et doivent s'installer automatiquement pour chaque [logiciel de traitement de texte pris en charge (page en anglais)](https://www.zotero.org/support/system_requirements#word_processor_plugins) installé sur votre ordinateur lorsque vous démarrez Zotero pour la première fois.

Vous pouvez réinstaller ultérieurement ces extensions depuis la rubrique Citer -&gt; Traitements de texte des paramètres de Zotero. Si vous rencontrez des difficultés, consultez les [instructions de dépannage (page en anglais)](https://www.zotero.org/support/word_processor_plugin_troubleshooting#zotero_toolbar_doesn_t_appear).

Si vous avez déjà installé les versions Firefox des extensions de traitement de texte dans Zotero 5.0 ou Zotero Standalone 4.0, vous devez les désinstaller à partir de Outils -&gt; Extensions.
