# Le connecteur Zotero

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Zotero connector](https://www.zotero.org/support/connector) - dernière mise à jour de la traduction : 2024-05-24*
</div>

Le [connecteur Zotero](https://www.zotero.org/download/connectors) est une extension de navigateur. Il vous aide à créer une bibliothèque de documents avec des métadonnées bibliographiques riches. Le connecteur Zotero est disponible pour Firefox, Chrome et Safari.

## Ajouter des documents

Une fois que vous avez [installé le connecteur](https://www.zotero.org/download/connectors), un bouton Zotero est ajouté dans votre navigateur. Cliquer sur ce bouton enregistre la page web ouverte en cours dans votre bibliothèque Zotero. Le processus d'enregistrement suit trois étapes.

### Sélectionnez une collection dans Zotero

![Sélection d'une collection dans le panneau de gauche de Zotero](./images/select_collection_FR.png){class="img150px"}

### Cliquez sur le bouton du connecteur

![Clic sur le bouton du connecteur Zotero pour lancer l'enregistrement](./images/save_button_article_FR.png){class="img500px"}  

### Le document est enregistré dans votre bibliothèque

![Message de progression de l'enregistrement affiché dans le navigateur, après un clic sur le bouton du connecteur Zotero](./images/progress_window_FR.png)

Pour des indications plus détaillées, consultez la page [Ajouter des documents à Zotero](./adding_items_to_zotero.md).

## Autres fonctionnalités

En plus de l'enregistrement des documents, le connecteur dispose d'autres fonctionnalités qui peuvent vous aider dans votre flux de travail.

### Détection des serveurs mandataires institutionnels

De nombreuses institutions fournissent comme moyen d'accéder aux ressources électroniques à distance (hors du campus) l'authentification via un serveur mandataire web (proxy). Le connecteur rend cela plus commode, en détectant automatiquement votre serveur mandataire institutionnel. Une fois que vous avez accédé à un site via un serveur mandataire institutionnel, le connecteur Zotero redirige ensuite automatiquement vos requêtes ultérieures vers ce site via ce serveur mandataire. Dans l'exemple ci-dessous, vous êtes automatiquement redirigé vers jstor.org.proxy.my-university.edu lorsque vous ouvrez un lien vers jstor.org.


![Message d'invitation à activer la redirection automatique vers le serveur mandataire affiché dans le navigateur par le connecteur Zotero](./images/proxy-register.png){class="img500px"}

La détection des serveurs mandataires ne nécessite pas de configuration manuelle. Vous pouvez la désactiver ou la personnaliser depuis les [préférences du connecteur Zotero](./connector_preferences.md). 

### Import de fichiers bibliographiques

De nombreuses bases de données en ligne offrent aux utilisateurs la possibilité d'exporter les références bibliographiques directement au format RIS ou BiBTex. Le connecteur Zotero vous invite automatiquement à ajouter ces références directement dans votre bibliothèque. Si vous choisissez "Cancel", vous pouvez télécharger le fichier normalement.

![Fenêtre de dialogue du connecteur Zotero affichée à la création d'un export de fichier bibliographique depuis une base de données, un moteur de recherche, etc.](./images/automatic_file_import.png){class="img500px"}

### Installation des styles bibliographiques

Le connecteur Zotero facilite l'installation des [styles bibliographiques](./styles.md) depuis le [dépôt des styles Zotero](https://www.zotero.org/styles). Cliquer sur l'intitulé d'un style l'installe automatiquement dans Zotero. Si vous choisissez "Cancel", vous pouvez télécharger le fichier normalement.


### Enregistrement dans la bibliothèque en ligne

Le connecteur Zotero vous permet d'enregistrer des documents directement dans votre bibliothèque en ligne sur zotero.org sans que Zotero soit ouvert. Vous devez privilégier le client Zotero local pour gérer votre bibliothèque et y enregistrer des documents, mais enregistrer dans la bibliothèque en ligne peut constituer une alternative si le client n'est pas disponible.

Lorsque vous utilisez le connecteur Zotero pour enregistrer un document sans que Zotero soit ouvert, vous êtes invité à fournir une autorisation pour votre compte zotero.org. Une fois l'autorisation accordée, le connecteur Zotero enregistre directement les documents dans votre bibliothèque en ligne.

![Message de progression de l'enregistrement dans la bibliothèque en ligne affiché dans le navigateur, après un clic sur le bouton du connecteur Zotero](./images/progress_window_zotero_org_FR.png)

Vous pouvez consulter votre bibliothèque en ligne en cliquant sur le lien "Web Library" affiché en haut de toutes les pages du [site zotero.org](https://www.zotero.org). Pour accéder depuis Zotero aux documents enregistrés, vous devez configurer la [synchronisation](./sync.md).


## Préférences

Consultez la page [Les préférences du connecteur Zotero](./connector_preferences.md) pour des indications détaillées.

### Localiser la page des préférences
#### Chrome

Vous pouvez trouver les préférences du connecteur soit par un clic-droit sur l'icône de l'extension "Zotero Connector" puis la sélection de "Options", soit en allant à *about:extensions* et en cliquant sur le lien "Options" sous l'extension "Zotero Connector".

#### Firefox

La page des préférences du connecteur est accessible soit par un clic-droit depuis n'importe quelle page, sous le sous-menu "Zotero Connector", soit en allant à *about:addons* et en cliquant sur le bouton "Preferences" sous l'extension "Zotero Connector".

#### Safari

Vous pouvez localiser la page des préférences du connecteur soit en appuyant longuement sur le bouton d'extension du connecteur et en sélectionnant "Zotero Preferences", soit en sélectionnant "Zotero Preferences" après un clic-droit depuis depuis n'importe quelle page.

### Sections

La page des préférences comporte les sections suivantes.

* [General](./connector_preferences.md/#onglet-general) : vérifier le statut du client Zotero et autoriser l'enregistrement vers zotero.org
* [Proxies](./connector_preferences.md/#onglet-proxies)  (non disponible sur Safari) : configurer et modifier les paramètres des serveurs mandataires institutionnels
* [Advanced](./connector_preferences.md/#onglet-advanced)  : options pour le dépannage, le signalement des erreurs et le débogage

![Les préférences du connecteur Zotero : onglet "General"](./images/connector_preferences_general.png){class="img500px"}
