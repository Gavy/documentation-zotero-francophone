# FAQ pour Zotero Storage

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Zotero Storage FAQ](https://www.zotero.org/support/storage_faq) - dernière mise à jour de la traduction : 2024-11-01*
</div>

## Qu'est-ce que Zotero Storage ?

Zotero Storage fournit un espace de stockage en ligne pour vos fichiers Zotero, vous permettant de :

* synchroniser des PDF, des images, des captures de page web et autres fichiers entre tous vos ordinateurs,
* partager les fichiers joints dans vos bibliothèques de groupe Zotero,
* et accéder aux fichiers via votre bibliothèque en ligne sur zotero.org.

Vous pouvez toujours enregistrer un nombre illimité de fichiers dans votre bibliothèque Zotero locale, avec ou sans Zotero Storage.

## Comment souscrire à Zotero Storage ?

Il vous suffit de vous rendre sur le profil de votre compte Zotero et de [sélectionner une offre de stockage](https://www.zotero.org/settings/storage).

## Comment souscrire à Zotero Storage pour l'ensemble de mon laboratoire, de mon université ou de mon entreprise ?

[Zotero Lab et Zotero Institution](https://www.zotero.org/storage/institutions) permettent aux membres de votre organisation de disposer d'un espace de stockage personnel et collectif illimité.

## Comment puis-je payer pour Zotero Storage ?

Diverses méthodes de paiement sont prises en charge, y compris les principales cartes de crédit et de débit, ainsi que plusieurs portefeuilles et options de virement bancaire en fonction de l'endroit où vous vous trouvez. Les offres Zotero Lab et Zotero Institution peuvent également être payées par virement bancaire.

## Que faire si je n'ai pas de carte de crédit ou de débit ou si ma carte ne fonctionne pas ?

Outre les cartes de crédit et de débit, plusieurs autres méthodes de paiement courantes dans certaines régions peuvent être utilisées. Si vous effectuez votre achat depuis l'UE, vous pouvez choisir de régler votre paiement en euros dans la boîte de dialogue de paiement, ce qui activera plusieurs méthodes de paiement spécifiques à l'euro.

Si vous ne disposez d'aucune des méthodes de paiement prises en charge ou si vous rencontrez des problèmes après avoir essayé de payer via la [page de vos paramètres de stockage](https://www.zotero.org/settings/storage), veuillez envoyer un courriel à [storage@zotero.org](mailto:storage@zotero.org). Indiquez votre nom d'utilisateur Zotero et le volume de stockage que vous souhaitez souscrire (2 Go, 6 Go ou illimité), ainsi que les méthodes de paiement que vous pouvez en général utiliser. Nous ferons de notre mieux pour vous aider à finaliser votre commande.

## Où puis-je trouver un reçu ou une facture pour mon paiement ?
Vous recevez automatiquement un avis de paiement par courriel de notre processeur de paiement (Stripe) lorsqu'un paiement a été effectué. 

Les reçus/factures détaillés de tous les paiements récents pour un abonnement de stockage individuel sont disponibles dans vos [paramètres de stockage](https://www.zotero.org/settings/storage). 

À partir d'août 2024, les factures payées pour les abonnements Lab sont disponibles sur la page de gestion du Lab.

## Les fichiers stockés dans mes groupes seront-ils librement accessibles aux autres membres de ces groupes ?

Oui, tous les groupes dont vous êtes propriétaire puiseront dans votre abonnement pour le stockage des fichiers joints. Tous les membres de ces groupes pourront accéder librement aux fichiers stockés.

## Comment répartir l'espace de stockage entre ma bibliothèque personnelle et mes groupes ?

Votre bibliothèque personnelle et les groupes dont vous êtes propriétaire puisent automatiquement leur espace de stockage dans votre abonnement. Les groupes dont vous n'êtes pas propriétaire puisent leur espace de stockage dans l'abonnement du propriétaire du groupe.

## Comment puis-je modifier mon offre de stockage actuelle ?

Vous pouvez changer l'offre de stockage à laquelle vous êtes actuellement abonné à tout moment à partir de vos [paramètres de stockage](https://www.zotero.org/settings/storage).

Lorsque vous passez à une nouvelle offre, vous n'êtes pas facturé immédiatement pour la nouvelle offre - votre date d'expiration est simplement ajustée au prorata du solde  de votre ancienne offre. Le premier prélèvement pour votre nouveau niveau d'abonnement sera effectué à votre nouvelle date d'expiration.

_Exemple : Nous sommes en février 2022, et il vous reste 6 mois sur un abonnement de 2 Go à 20 $, avec une date d'expiration en août 2022. (6 mois ÷ 12 mois) x 20 $ = 10 $ de valeur restante. Si vous passez à l'abonnement de 6 Go à 60 $, nous appliquons ces 10 $ à votre nouveau niveau d'abonnement pour fixer une nouvelle date d'expiration. (10 $ ÷ 60 $) * 12 mois = 2 mois, votre nouvelle date d'expiration sera donc dans deux mois, en avril. Vous n'avez payé que les 20 $ d'origine en août dernier, et vous serez facturé 60 $ pour la première fois en avril._

(La seule exception concerne le cas où il vous reste moins de deux semaines d'abonnement. Dans ce cas, le solde inutilisé est toujours appliqué à votre nouvel abonnement, mais nous vous facturerons la nouvelle offre immédiatement).

## Mon abonnement sera-t-il renouvelé automatiquement ?
Lorsque vous souscrivez un abonnement de stockage, vous pouvez choisir de le renouveler ou non automatiquement chaque année. Vous pouvez annuler le renouvellement automatique à tout moment à partir de vos [paramètres de stockage](https://www.zotero.org/settings/storage). Vous recevrez également un courriel de rappel lorsque votre abonnement arrivera à expiration ou sera sur le point d'être renouvelé automatiquement.

## Comment puis-je annuler mon abonnement ?

Si vous avez choisi de renouveler votre abonnement automatiquement, vous pouvez annuler le prochain renouvellement à partir de vos [paramètres de stockage](https://www.zotero.org/settings/storage). Comme indiqué dans les [conditions de service](https://www.zotero.org/support/terms/terms_of_service), les comptes payants ne sont pas remboursables. Nous appliquons cette politique parce que les coûts encourus de notre côté fluctuent considérablement en fonction des types d'usage individuels.

## Pourquoi des taxes sur les ventes ou la TVA sont-elles appliquées ?
Les abonnements de stockage Zotero sont soumis à des taxes sur la consommation dans de nombreuses juridictions. Selon votre lieu de résidence, il peut s'agir de la taxe sur la valeur ajoutée (TVA), de la taxe sur les produits et services (TPS) ou de la taxe à la consommation (TC). 

Lorsque vous achetez un abonnement à Zotero, la taxe correspondante vous est facturée en fonction de votre adresse de facturation. 

La taxe est affichée en tant que ligne de facture indépendante sur l'écran de paiement lorsque vous achetez, mettez à niveau ou renouvelez votre abonnement. 

## Comment puis-je appliquer mon exonération de taxe professionnelle à mon achat ? 
Les entreprises non enregistrées aux États-Unis peuvent être exemptées du paiement des taxes à la consommation. En tant qu'entreprise assujettie à la taxe, veuillez cocher la case "Ajouter un numéro d'identification fiscale" pour saisir un numéro de TVA ou de TPS valide et éviter que des taxes ne vous soient facturées sur vos achats. La taxe vous sera facturée si vous n'avez pas saisi de données fiscales dans notre système. 

## Mon organisation est exonérée de la taxe sur les ventes dans mon État. Comment puis-je appliquer cette exonération à mon abonnement ? 

Pour les abonnements Zotero Lab ou Zotero Institution, vous pouvez nous envoyer par courriel à [storage@zotero.org](mailto:storage@zotero.org) une copie de votre certificat d'exonération de la taxe sur les ventes ou tout autre document pertinent. Cette démarche doit être effectuée avant l'achat. 

Les abonnements individuels au stockage Zotero sont destinés à être vendus à des particuliers. Nous ne sommes pas en mesure de gérer les exemptions de taxe sur les ventes pour les abonnements de stockage individuels.