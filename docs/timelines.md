# Les chronologies

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Timelines](https://www.zotero.org/support/timelines) - dernière mise à jour de la traduction : 2024-09-06*
</div>

La fonctionnalité de création de chronologies a été supprimé de Zotero 7. 