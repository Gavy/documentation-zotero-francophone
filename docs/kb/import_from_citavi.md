# Comment puis-je importer depuis Citavi ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How can I import from Citavi?](https://www.zotero.org/support/kb/import_from_citavi) - dernière mise à jour de la traduction : 2023-04-26*
</div>


Le meilleur format pour importer des notices de Citavi dans Zotero est Citavi XML. Ce format permet d'importer les métadonnées bibliographiques de l'article ainsi que les citations, les tâches, les pièces jointes, etc. À partir de Zotero 6, Zotero peut également importer les annotations des PDF. Zotero peut importer des fichiers exportés depuis Citavi 5 et Citavi 6.

## Format Citavi XML (recommandé)
### Cas particulier : Projet cloud

Si votre projet est un projet cloud, vous devez en faire une copie locale pour préparer l'exportation.

1. Dans Citavi, cliquez sur `File` → `This project` → `Save a copy of this project…`
2. Dans la boîte de dialogue suivante, cliquez sur `Create local project copy`, indiquez un nom significatif (par exemple *monprojet_local*) dans le champ `Project name` et cliquez sur `Next`.
3. Dans la boîte de dialogue suivante, cliquez sur `Keep the cloud project` et cliquez sur `Next`.
4. Dès que la copie locale est terminée, le projet local s'ouvre dans une nouvelle fenêtre Citavi. A partir de là, continuez selon la procédure suivante.

### Projet local

1. Dans Citavi, cliquez sur `File` → `This project` → `Create backup…`
2. La sauvegarde est enregistrée dans un fichier se terminant par `ctv5bak` ou `ctv6bak` et se trouve normalement dans votre [répertoire personnel](https://fr.wikipedia.org/wiki/R%C3%A9pertoire_personnel) sous `Documents\Citavi 5\Backup` ou `Documents\Citavi 6\Backup` (c'est-à-dire, suivant le format `C:\Users\<username>\Documents\Citavi 5\Backup\<projectname>`). Vous pouvez également rechercher le dossier de sauvegarde sous Outils / Options / Dossiers et cliquer sur "Ouvrir le dossier avec l'explorateur Windows", voir [les instructions de sauvegarde dans le manuel Citavi](https://www.citavi.com/sub/manual5/en/101_backing_up_your_projects.html).
3. Le fichier `ctv5bak` ou `ctv6bak` est un [fichier zip](https://fr.wikipedia.org/wiki/ZIP_(format_de_fichier)) qui ne peut pas être traité directement - vous devez d'abord le décompresser et continuer avec le fichier décompressé résultant. Pour le décompresser, il faut soit changer l'extension en .zip, soit utiliser un logiciel dédié à l'archivage/désarchivage de fichiers, tel que 7zip. Notez que les extensions de fichiers sont souvent cachées par le système d'exploitation. Pour modifier l'extension, vous devrez peut-être activer la visibilité des extensions de fichiers.
4. Pour importer des fichiers joints (par exemple des PDF) dans Zotero, vous devez vous assurer qu'ils se trouvent dans le même dossier que le fichier `ctv5` ou`ctv6` que vous importez. Citavi enregistre toutes les pièces jointes dans le dossier du projet (par exemple, `C:\Users\<nom d'utilisateur>\Documents\Citavi 5\Projects\<nom du projet>\Citavi Attachments`). Copiez-les à partir de là.
5. Importez le fichier `ctv5` ou `ctv6` dans Zotero.

## Limitations

* Les collections imbriquées dans Citavi ne seront plus imbriquées dans Zotero. Cependant, les nombres associés aux noms de collections devraient représenter l'imbrication et ajuster cela manuellement par la suite devrait être facile.
* Pour importer les annotations des PDF, vous devez utiliser Zotero 6.

## Format RIS (alternative)

Comme alternative à la procédure ci-dessus, vous pouvez exporter votre bibliothèque Citavi au format RIS. Ce format ne conservera que les métadonnées bibliographiques des documents. Aucun élément supplémentaire, tel que les citations provenant des fonctionnalités de "knowledge management" de Citavi, ne sera inclus.
