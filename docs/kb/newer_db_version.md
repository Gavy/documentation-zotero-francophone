# Pourquoi est-ce que j'obtiens une erreur de version de base de données (database version error) ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Why am I getting a database version error?](https://www.zotero.org/support/kb/newer_db_version) - dernière mise à jour de la traduction : 2024-10-05*
</div>

Après avoir réinstallé Zotero ou rétrogradé de version, vous pouvez voir apparaître le message d'erreur suivant lorsque vous essayez d'ouvrir Zotero.

`This version of Zotero is older than the version last used with your database. Please upgrade to the latest version from zotero.org.`

Ce message s'affiche quand on tente d'utiliser dans une version antérieure de Zotero une base de données Zotero créée avec une version ultérieure du logiciel. Par exemple, si vous avez installé Zotero 5.0 mais ensuite réinstaller Zotero 4.0.29.15, vous obtiendrez cette erreur. La plupart des versions de Zotero assure la rétrocompatibilité de la base de données, mais occasionnellement il est nécessaire pour les développeurs de Zotero de faire des changements dans la base de données qui la rende incompatible avec les versions antérieures.

La meilleure solution est généralement de réinstaller la dernière version depuis [zotero.org](https://www.zotero.org/download). (Si vous utilisiez Zotero Beta - la version en développement -, vous devrez peut-être installer la [dernière version beta](https://www.zotero.org/support/beta_builds).)

Si vous ne voulez pas ou ne pouvez pas installer la dernière version, vous pouvez restaurer la sauvegarde de pré-mise à jour de la base de donnée que Zotero stocke dans votre [répertoire de données Zotero](../zotero_data.md). Cherchez le fichier `zotero.sqlite.*.bak` avec le nombre le plus élevé (par exemple `zotero.sqlite.77.bak`). Fermez Zotero, enlevez votre fichier existant `zotero.sqlite` et copiez à la place le fichier de sauvegarde numéroté à la place, en le renommant `zotero.sqlite`. Puis redémarrez Zotero.

Si vous n'avez pas de données qu'il vous importe de garder, ou si vous avez synchronisé l'intégralité de vos données et fichiers dans votre compte en ligne, vous pouvez conserver votre version actuelle de Zotero en fermant Firefox puis en supprimant complètement le [répertoire de données Zotero](../zotero_data.md), et éventuellement synchronisant ensuite vos données depuis le compte en ligne.

**Auparavant, la cause la plus fréquente de cette erreur était d'avoir utilisé Juris-M (anciennement connu sous le nom de MLZ).** La version actuelle de ce logiciel ne provoque plus ce problème. Voir [ce message](https://forums.zotero.org/discussion/50443/multilinguale-version-von-zotero-mlz/?Focus=228336#Comment_228336) du développeur de Juris-M sur la façon de revenir à l'utilisation normale de Zotero en toute sécurité.