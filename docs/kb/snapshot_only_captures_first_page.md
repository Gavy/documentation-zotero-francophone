# Une capture de page web n'enregistre que la première des multiples pages d'un article du New York Times. Comment faire pour que Zotero capture l'intégralité de l'article ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [A snapshot only captures the first of multiple pages of a New York Times article. How do I get Zotero to capture the full article?](https://www.zotero.org/support/kb/snapshot_only_captures_first_page) - dernière mise à jour de la traduction : 2023-04-06*
</div>

La fonctionnalité de capture de page web n'enregistre que la page en cours. De nombreux sites, comme le New York Times, n'affichent qu'une partie du document qui vous intéresse sur la première page. Une règle générale pour contourner ce problème est de rechercher une version imprimable. Vous obtenez ainsi une page contenant l'intégralité du document. Vous pouvez alors capturer le texte intégral en cliquant sur le bouton d'enregistrement du connecteur Zotero, qui affiche une icône de journal. Le New York Times dispose également d'un bouton "Single Page". Il existe même un script [greasemonkey](https://addons.mozilla.org/firefox/addon/748) qui [affiche automatiquement](http://userscripts.org/scripts/show/56690) la vue sur une page unique.
