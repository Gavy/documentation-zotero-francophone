# Puis-je quand même utiliser Zotero si je ne peux pas installer de programmes sur mon ordinateur ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Can I still use Zotero if I can't install programs on my computer?](https://www.zotero.org/support/kb/portable_zotero) - dernière mise à jour de la traduction : 2024-08-06*
</div>

 Sous Windows, vous pouvez annuler l'invite d'administration lorsque vous installez Zotero, et il effectuera une installation locale que vous aurez le droit de mettre à jour en tant qu'utilisateur local.

Nous fournissons également une version ZIP de Zotero pour Windows que vous pouvez extraire et exécuter à partir de n'importe quel dossier. La version ZIP n'enregistre pas automatiquement Zotero comme gestionnaire de différents types de fichiers (par exemple, les fichiers RIS), nous recommandons donc d'utiliser le programme d'installation si possible.

Sous macOS et Linux, vous pouvez exécuter l'application à partir de n'importe quel dossier (par exemple, un dossier Applications dans votre dossier personnel sur macOS au lieu de /Applications). 