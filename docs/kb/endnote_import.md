# Comment puis-je importer depuis EndNote ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do I import from EndNote?](https://www.zotero.org/support/kb/endnote_import) - dernière mise à jour de la traduction : 2024-06-07*
</div>

## Exporter votre bibliothèque depuis EndNote

Zotero ne peut pas importer directement des bibliothèques EndNote au format ".enl", ainsi la première étape consiste à exporter votre bibliothèque depuis EndNote. Le meilleur format d'export pour cela est XML.

Il peut être nécessaire, pour d'anciennes bibliothèques EndNote, de convertir les figures en fichiers joints avant d'exporter un fichier RIS. Il faut pour ce faire aller dans le menu References -> Figure et sélectionner "Convert Figures to File Attachments..."

1.  Si vous souhaitez exporter un sous-ensemble de votre bibliothèque EndNote, sélectionnez les entrées que vous souhaitez exporter.
2.  Allez dans le menu File -> Export. Une boîte de dialogue s'affiche pour vous demander où sauvegarder le fichier d'export.
3.  Naviguez jusqu'à votre répertoire de données EndNote (en général, Mes Documents\\endnote.Data). Ce répertoire contient un dossier 'PDF', mais vous devez vous assurer de sélectionner le répertoire de données plutôt qu'un sous-répertoire.
     * **Ceci est important!** Zotero recherche les fichiers joints dans un répertoire relatif à l'emplacement du fichier XML exporté. Si vous enregistrez ce fichier au mauvais endroit, les fichiers joints ne seront pas inclus lors de l'import dans Zotero.
4. Pour "Save as type:", choisissez "XML".
5. Si vous ne souhaitez exporter qu'un sous-ensemble de votre bibliothèque, cochez la case "Export Selected References". Sinon, assurez-vous qu'elle n'est pas cochée.
6. Cliquez sur "Save".
7. Fermez EndNote.

## Importer dans Zotero

Si vous n'importez pas dans une bibliothèque vide, nous **vous recommandons vivement** de [faire une sauvegarde de votre répertoire de données Zotero](../zotero_data.md#sauvegarder-votre-bibliotheque-zotero). Cela vous évitera de la frustration si la façon dont votre bibliothèque a été transférée ne vous convient pas. Dans ce cas, il suffira de [restaurer votre bibliothèque à partir d'une sauvegarde](../zotero_data.md#restaurer-votre-bibliotheque-zotero-a-partir-dune-sauvegarde).

Vous devez également désactiver temporairement la synchronisation automatique dans le volet [Synchronisation](../preferences_sync.md) des préférences de Zotero. Après avoir importé votre bibliothèque et vérifié que vous êtes satisfait des données importées, vous pouvez réactiver la synchronisation automatique.

Dans Zotero, cliquez sur "Importer..." dans le menu "Fichier". Une boîte de dialogue apparaît et vous demande de sélectionner le fichier à importer. Naviguez jusqu'à l'emplacement où vous avez exporté votre bibliothèque EndNote (si vous avez suivi les instructions ci-dessus, ce devrait être Mes Documents\\endnote.Data) et sélectionnez le fichier XML. Cliquez sur "Ouvrir".

Notez que si Zotero rencontre dans les données Endnote XML des champs qu'il ne prend pas en charge (champs personnalisés, adresse ou affiliation de l'auteur par exemple), il ajoute ces données à une note jointe au document importé. Le marqueur "\_EndnoteXML import" est associé aux notes ainsi générées. Si l'import ajoute beaucoup de notes de ce type, la performance de Zotero peut être affectée négativement. Vous devez passer en revue chacune de ces notes pour déterminer si les données doivent être conservées et supprimer toutes les notes superflues. De plus, vous devez vérifier ces notes pour déterminer si des données peuvent être migrées vers des champs Zotero appropriés : cela est particulièrement important si vous utilisiez des champs EndNote de façon non standard.

Vous pouvez rapidement afficher toutes les notes générées lors de l'import en cliquant sur le marqueur "\_EndnoteXML import" dans le sélecteur de marqueurs dans le coin inférieur gauche de la fenêtre Zotero. Vous pouvez supprimer rapidement toutes ces notes en sélectionnant le marqueur dans le sélecteur de marqueurs, en cliquant dans la liste des documents et en tapant Cmd+A (Mac) ou Ctrl+A (Windows/Linux) pour sélectionner tous les documents correspondants, puis en choisissant "Mettre les documents à la corbeille..." dans le menu contextuel affiché par le clic droit.

## Obtenir de l'aide

Si vous rencontrez des difficultés liées à l'import et à l'export de références, n'hésitez pas à demander de l'aide sur les [forums Zotero](https://forums.zotero.org).

## Format RIS (alternative)

Il est également possible d'exporter des documents de EndNote en utilisant le format RIS au lieu du format Endnote XML. Le seul avantage de RIS sur XML est que l'ID de la base de données EndNote peut être conservé pour chaque document. Si cela est nécessaire, ouvrez alors dans Zotero le volet Avancées des [préférences de Zotero](./preferences.md) et cliquez sur le bouton "Editeur de configuration" dans l'onglet "Générales". Recherchez "RIS.import.keepID" et double-cliquez dessus pour définir la valeur sur **true**. Si vous voulez que les données des champs inconnus soient conservées dans des notes (comme décrit ci-dessus), recherchez également "RIS.import.ignore.unknown" et définissez la valeur sur **false**.

Dans EndNote, exportez votre bibliothèque comme détaillé ci-dessus, en paramétrant "Save as type:" sur "Text file (.txt)". Réglez "Output style:" sur "RefMan (RIS)". Naviguez jusqu'à votre répertoire de données EndNote, enregistrez le fichier d'export, puis importez dans Zotero comme décrit précédemment.

Avec cette configuration, RIS conserve les ID de base de données EndNote, mais les italiques, les caractères gras ou tout autre mise en forme définie dans les champs sont perdus. XML doit toujours être utilisé, à moins que des ID de base de données Endnote ne soient nécessaires pour une raison précise.
