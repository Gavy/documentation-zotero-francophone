# Sur l'application de bureau Zotero, comment puis-je passer à un autre compte Zotero ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How can I switch the Zotero desktop app to a different Zotero account?](https://www.zotero.org/support/kb/switching_accounts) - dernière mise à jour de la traduction : 2023-01-20*
</div>

Une base de données Zotero ne peut être synchronisée qu'avec un seul compte Zotero.

Une fois que vous avez configuré la synchronisation pour une base de données déterminée dans le volet "Synchronisation" des préférences de Zotero, il n'est plus possible de synchroniser cette base de données avec un autre compte. Si vous dissociez votre compte et tentez de configurer la synchronisation avec un autre compte, Zotero vous avertira que toutes les données locales seront supprimées si vous continuez. Cela empêche que des données provenant de différents comptes ne soient combinées par inadvertance, et cela évite des conflits difficiles à résoudre entre des données créées dans plusieurs comptes.

* Si vous souhaitez simplement changer de nom d'utilisateur, vous pouvez le faire à partir des [paramètres de votre compte en ligne](https://www.zotero.org/settings/account) plutôt que de passer à un tout nouveau compte. (Si vous avez déjà créé un nouveau compte avec le nom d'utilisateur souhaité, vous devrez le supprimer ou modifier son nom d'utilisateur avant de pouvoir utiliser ce nom d'utilisateur pour votre compte initial).

* Si vous souhaitez utiliser les bibliothèques de plusieurs comptes sur le même ordinateur, vous pouvez configurer [plusieurs profils Zotero](./multiple_profiles.md) pointant vers des répertoires de données distincts et synchroniser chacun d'eux avec un compte différent.

* Si vous avez créé des données dans plusieurs comptes et que vous souhaitez les fusionner dans un seul compte, vous pouvez exporter au format Zotero RDF les données et les fichiers d'un compte - soit sur un autre ordinateur, soit à partir d'un profil distinct que vous supprimerez par la suite - et les importer dans l'autre compte. Notez que l'utilisation de l'exportation et de l'importation réinitialisera les dates d'ajout et de modification et brisera les liens vers les citations Zotero dans vos documents de traitement de texte existants. Il est par conséquent préférable d'exporter à partir du compte contenant le moins de données, ou les données avec lesquelles vous avez le moins interagi.