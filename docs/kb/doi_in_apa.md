# Format du DOI dans le style APA

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [DOI format in APA style](https://www.zotero.org/support/kb/doi_in_apa) - dernière mise à jour de la traduction : 2023-03-22*
</div>

Depuis l'introduction du système d'identifiant d'objet numérique (DOI) en 2000, les directives sur la meilleure façon d'afficher les DOI ont évolué. Depuis mars 2017, Crossref, une agence d'enregistrement de DOI influente, [recommande](https://www.crossref.org/display-guidelines/) désormais le format suivant :

`https://doi.org/10.1037/rmh0000008`

Notez l'utilisation de "https" au lieu de "http", et de "doi.org" au lieu de "dx.doi.org".

Alors que la 6e édition du [*Publication Manual of the American Psychological Association*](http://www.apa.org/pubs/books/4200066.aspx), publiée en 2010, recommandait le format original `doi:10.1037/rmh0000008`, [l'APA a mis à jour ses lignes directrices](http://blog.apastyle.org/apastyle/2017/03/doi-display-guidelines-update-march-2017.html) pour suivre les lignes directrices de Crossref concernant l'affichage des DOI avec ce dernier format.

Les styles CSL de la 6e édition de l'APA ont donc été mis à jour pour utiliser également le format `https://doi.org/10.1037/rmh0000008`.

## Bref historique des formats du DOI

[Comme l'explique Crossref dans ses lignes directrices](https://www.crossref.org/display-guidelines/#why-not-use-doi-or-doi), le format concis original `doi:10.1037/rmh0000008` a été recommandé dans l'espoir que les navigateurs web reconnaîtraient un jour automatiquement ces DOI et établiraient des liens hypertextes.

Comme cela ne s'est pas produit, Crossref a [modifié sa recommandation en 2011](https://www.crossref.org/news/2011-08-02-crossref-revises-doi-display-guidelines/)  pour afficher les DOI sous la forme d'URL normales, au format `http://dx.doi.org/10.1037/rmh0000008`. L'APA a commencé à recommander ce format plus récent (mais aujourd'hui également dépassé) dans son guide d'accompagnement de 2012, [*APA Style Guide to Electronic References*](http://www.apastyle.org/products/4210512.aspx), et a discuté de ce changement dans un [billet de blogue](http://blog.apastyle.org/apastyle/2014/07/how-to-use-the-new-doi-format-in-apa-style.html) de 2014.

Plus récemment, il y a eu un fort mouvement pour faire passer le web du protocole HTTP au protocole HTTPS, plus sûr. Des changements techniques ont également permis de lier les DOI via le plus court `doi.org` au lieu de `dx.doi.org`. L'ensemble de ces changements a incité Crossref à [modifier  le format recommandé](https://www.crossref.org/blog/new-crossref-doi-display-guidelines-are-on-the-way/) pour `https://doi.org/10.1037/rmh0000008`.
