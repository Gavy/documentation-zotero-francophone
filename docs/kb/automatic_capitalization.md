# Comment désactiver les changements automatiques de casse/majuscules lors de l'importation d'un document ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do I turn off automatic case changes/capitalization during item import?](https://www.zotero.org/support/kb/automatic_capitalization) - dernière mise à jour de la traduction : 2023-01-18*
</div>

Par défaut, Zotero essaie de nettoyer la casse et les majuscules dans les titres des documents lors de leur importation (par exemple, les titres TOUT EN MAJUSCULES sont convertis avec les Initiales en Majuscules). Pour désactiver cette fonction, suivez les étapes suivantes.

1. Dans le [volet avancé des préférences de Zotero](../advanced.md), dans l'onglet "Générales", cliquez sur le bouton "Editeur de configuration".
2. Trouvez "extensions.zotero.capitalizeTitles" dans la liste. Pour le trouver rapidement, vous pouvez taper "capitalize" dans la boîte de filtre.
3. Double-cliquez sur "extensions.zotero.capitalizeTitles" pour passer la valeur de "true" à "false".