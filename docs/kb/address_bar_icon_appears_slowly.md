# Pourquoi l'icône d'un livre/article/etc. n'apparaît-elle pas tout de suite dans la barre d'adresse ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Sometimes the icon for a book/article/etc doesn’t appear in the address bar right away. What’s going on?](https://www.zotero.org/support/kb/address_bar_icon_appears_slowly) - dernière mise à jour de la traduction : 2022-12-19*
</div>

Zotero doit attendre le chargement de la page entière avant de pouvoir vérifier la présence d'un document sur la page. Parfois, certains éléments de la page (une grande image, par exemple) prennent du temps à se charger et retardent ainsi l'activité de Zotero.

Si le chargement de la page est terminé, mais que vous ne voyez toujours pas l'icône du type de document approprié, voir [Pourquoi le connecteur Zotero ne propose-t-il pas d'enregistrer les données complètes d'une page web ?](./page_not_recognized.md).
