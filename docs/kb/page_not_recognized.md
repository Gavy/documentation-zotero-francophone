# Pourquoi le connecteur Zotero ne propose-t-il pas d'enregistrer des données complètes à partir d'une page web?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Why doesn't the Zotero Connector offer to save complete data from a webpage?](https://www.zotero.org/support/kb/page_not_recognized) - dernière mise à jour de la traduction : 2024-08-28*
</div>

Le connecteur Zotero détecte les informations sur les pages Web via les *convertisseurs de site*. Si un convertisseur de site reconnaît la page consultée, le bouton d'enregistrement dans la barre d'outils du navigateur affiche l'icône d'un type de document donné (livre, article de revue, etc.) :

![Le bouton du connecteur Zotero affiche l'icône "livre" sur la page d'un livre sur Amazon](../images/save_button_book.png){ align=left }{class="img300px"}

Sur toutes les autres pages Web, le connecteur Zotero affiche une icône de page grise :

![Le bouton du connecteur Zotero affiche une icône de page grise sur une page Web](../images/save_button_webpage.png){ align=left }{class="img300px"}

Vous pouvez survoler le bouton pour voir quel convertisseur, le cas échéant, Zotero utilise pour enregistrer la page.

Les convertisseurs de Zotero doivent fonctionner avec la plupart des catalogues de bibliothèques, des sites Web populaires tels qu'Amazon et le New York Times, et avec de nombreuses bases de données fermées. Reportez-vous à la page [Convertisseurs Zotero](../translators.md) pour davantage d'exemples. Si un site n'est pas actuellement pris en charge ou si un convertisseur ne fonctionne pas, vous pouvez toujours cliquer sur l'icône grise pour enregistrer les informations de base de la page dans votre bibliothèque Zotero, mais vous devrez peut-être compléter certaines informations que Zotero n'aura pas pu détecter automatiquement.

Si vous voyez  seulement une icône de page Web sur les pages de produits Amazon ou les articles du NY Times (pas les pages d'accueil des sites), consultez la page [Dépannage des problèmes d’enregistrement dans Zotero](../troubleshooting_translator_issues.md).

Si vous voyez l'icône d'un type de document sur les pages de de produits Amazon et les articles du NY Times mais que vous voyez une icône de page Web sur un autre site pris en charge, vous pouvez le signaler dans les forums. Assurez-vous de fournir un exemple d'URL.
