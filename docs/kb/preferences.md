# Comment puis-je modifier les paramètres de Zotero?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do I change Zotero settings?](https://www.zotero.org/support/kb/preferences) - dernière mise à jour de la traduction : 2025-02-16*
</div>

Les paramètres de Zotero se trouvent dans le menu "Zotero" sur Mac et dans le menu "Édition" sur Windows et Linux.