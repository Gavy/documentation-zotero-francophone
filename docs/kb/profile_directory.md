# Emplacement du répertoire de profil Zotero

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Profile directory location](https://www.zotero.org/support/kb/profile_directory) - dernière mise à jour de la traduction : 2025-01-23*
</div>

Le système de profils de Zotero est basé sur [le système de profils de Firefox](https://support.mozilla.org/fr/kb/profils-la-ou-firefox-conserve-donnees-utilisateur).

Vous pouvez trouver le répertoire de votre profil Zotero à l'emplacement suivant :

## Mac	

`/Users/<Nom d’utilisateur>/Library/Application Support/Zotero/Profiles/<chaîne aléatoire>`

Note : le dossier `/Users/<Nom d’utilisateur>/Library` est caché par défaut. Pour y accéder, cliquez sur votre bureau, maintenez la touche Option enfoncée et cliquez sur le menu Go du Finder, puis sélectionnez Library dans le menu.

## Windows 11/10/8/7/Vista	

`C:\Users\<Nom d’utilisateur>\AppData\Roaming\Zotero\Zotero\Profiles\<chaîne aléatoire>`

Remarque : si AppData est caché sur votre système, cliquez sur la barre de recherche (ou sur Démarrer avant Windows 10), tapez `%appdata%` et appuyez sur Entrée, ce qui devrait vous amener au répertoire AppData\Roaming. Vous pouvez alors ouvrir le reste du chemin.

## Windows XP/2000	

`C:\Documents and Settings\<Nom d’utilisateur>\Application Data\Zotero\Zotero\Profiles\<chaîne aléatoire>`

## Linux	

`~/.zotero/zotero/<chaîne aléatoire>`


