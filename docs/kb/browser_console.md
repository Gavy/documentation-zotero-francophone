# Comment trouver la console du navigateur ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do I find the browser console?](https://www.zotero.org/support/kb/browser_console) - dernière mise à jour de la traduction : 2024-09-20*
</div>

 La console du navigateur est souvent utilisée pour identifier les problèmes d'une page web. Les développeurs de Zotero peuvent vous demander de vérifier la présence d'erreurs dans la console de votre navigateur pour vous aider à résoudre un problème avec la bibliothèque en ligne ou ZoteroBib.  

 Vous pouvez ouvrir la console de votre navigateur en utilisant la combinaison de touches suivante.

* Chrome et Edge :
  * MacOS : Cmd + Option + J
  * Windows/Linux : Ctrl + Maj + J
* Firefox :
  * MacOS : Cmd + Option + K
  * Windows/Linux : Ctrl + Maj + K
* Safari sur MacOS :
  * Cmd + Option + C
