#  Pourquoi une citation n'est-elle pas mise à jour dans mon document, alors que j'ai modifié la notice correspondante dans ma bibliothèque Zotero ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Why is a citation not updated in my document after editing the item in Zotero?](https://www.zotero.org/support/kb/citations_not_updating) - dernière mise à jour de la traduction : 2023-08-24*
</div>

Lorsque vous apportez des modifications aux données d'une notice (titre, auteur, date, etc.) dans votre bibliothèque Zotero, ces modifications sont reflétées dans les citations correspondantes dans votre traitement de texte dès que vous cliquez sur le bouton "Refresh" de l’extension de traitement de texte.

Si vos citations sont du texte plat et ne sont pas détectées du tout, consultez la page [Pourquoi Zotero ne détecte-t-il pas mes citations existantes ?](./existing_citations_not_detected.md).

Si vous avez toujours des citations actives (par exemple surlignées en gris lorsque vous cliquez dessus dans Word ou LibreOffice, ou affichant la fenêtre contextuelle "Edit in Zotero" dans Google Docs) mais que les modifications que vous effectuez dans Zotero ne sont pas reflétées dans votre document de traitement de texte, deux explications sont possibles.

* Soit vous avez modifié manuellement le texte de la citation et indiqué à Zotero de ne pas appliquer de mises à jour ultérieures ;
* soit la citation dans votre document n'est plus liée à la notice correspondante dans votre bibliothèque Zotero. 

Cliquez sur la citation, puis cliquez sur "Add/Edit Citation". Si vous avez modifié la citation, Zotero vous informera que le citation a été modifiée et vous proposera de la réinitialiser, après quoi cette citation se mettra à jour automatiquement. D'une manière générale, vous devez éviter les modifications manuelles dans le document de traitement de texte et [personnaliser la citation](../word_processor_plugin_usage.md#personnaliser-les-citations) pour ajouter des numéros de page, des préfixes, etc.

Si la citation n'a pas été modifiée, la boîte de dialogue de citation s'affichera et vous présentera la citation. Pour vérifier que la citation est toujours liée à votre bibliothèque, cliquez sur la bulle bleue et cherchez le bouton "Ouvrir dans Ma Bibliothèque [ou le nom de la bibliothèque du groupe]" dans la fenêtre contextuelle.

![Boîte de dialogue de citation : ajouter un préfixe et un suffixe, bouton "Ouvrir dans Ma Bibliothèque"](../images/citation-dialog-affixes-5_FR.png)

Si le bouton "Ouvrir dans..." n'apparaît pas, la citation n'est liée à aucune de vos bibliothèques Zotero et Zotero utilise les métadonnées de l'article intégrées dans le document pour générer la citation et l'entrée de bibliographie. Vous devrez supprimer la citation de votre document et l'insérer à nouveau, en veillant à la sélectionner dans la section des bibliothèques plutôt que dans la section "Cité" de la boîte de dialogue de citation .

Les citations peuvent devenir orphelines pour différentes raisons.

1. Vous avez des notices en double dans votre bibliothèque, vous avez cité l'un des doublons, puis vous l'avez supprimé au lieu de le fusionner avec l'autre notice en doublon.
2. Vous avez supprimé une notice de votre bibliothèque, puis vous l'avez importée à nouveau.
3. Vous avez cité une référence avec Mendeley et vous avez ensuite modifié le document avec Zotero (Zotero peut toutefois [rétablir les liens de citation Mendeley Desktop](./mendeley_import.md#utilisation-des-citations-de-mendeley) après l'import de votre bibliothèque Mendeley).