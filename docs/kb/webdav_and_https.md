# Pourquoi ne puis-je pas me connecter à un serveur WebDAV via HTTP sur iOS ou Android ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Why can't I connect to a WebDAV server via HTTP on iOS or Android?](https://www.zotero.org/support/kb/webdav_and_https) - dernière mise à jour de la traduction : 2024-09-20*
</div>

Dans les versions actuelles d'iOS et d'Android, les connexions non HTTPS sont bloquées par défaut. Les applications qui souhaitent autoriser les connexions non chiffrées doivent le faire spécifiquement et énumérer les domaines pour lesquels elles les autorisent, et les applications peuvent faire l'objet d'un examen plus approfondi. Dans la mesure où tout le monde peut obtenir un certificat gratuit par l'intermédiaire de [Let's Encrypt](https://letsencrypt.org/), nous avons choisi de nous en tenir aux valeurs par défaut et de ne pas compromettre la sécurité des connexions à partir de l'application. Ces connexions peuvent impliquer divers services en amont pour la récupération des métadonnées. Si elles étaient accidentellement effectuées par HTTP, cela constituerait un risque pour la confidentialité. (Nous avons également constaté des cas flagrants de redirection d'hôtes WebDAV HTTPS vers HTTP ; la configuration par défaut les bloque, comme il se doit.) 

iOS autorise les connexions non chiffrées vers des plages d'adresses IP privées (par exemple, 192.168.*). Android ne semble pas faire de même, mais nous avons ajouté comme exceptions `.local` et `.home.arpa`.
