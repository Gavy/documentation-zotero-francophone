# Comment joindre un fichier ou une page web à un document ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do I attach a file or web page to an item?](https://www.zotero.org/support/kb/storing_files) - dernière mise à jour de la traduction : 2022-12-19*
</div>

Après avoir ajouté un document à votre bibliothèque, sélectionnez-le et cliquez sur l'icône de trombone "Ajouter une pièce jointe" dans la barre d'outils de Zotero, ou affichez le menu contextuel en effectuant un clic-droit. Un menu déroulant vous permettra de choisir parmi l'une des options suivantes :

* ajouter un lien vers un fichier stocké ailleurs sur votre disque dur : "Joindre un lien vers un fichier...",
* copier un fichier dans votre répertoire de stockage Zotero : "Joindre une copie enregistrée d'un fichier...",
* ajouter un lien vers un emplacement en ligne : "Joindre un lien vers un URI...".

Vous pouvez également faire glisser le fichier depuis le système de fichiers de votre ordinateur. Maintenez la touche Maj [Mac] ou Ctrl [Windows/Linux] enfoncée tout en faisant glisser le fichier pour le déplacer plutôt que le copier.
