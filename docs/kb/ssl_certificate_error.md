# "[domaine] utilise un certificat de sécurité non valide."

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : ["[domain] uses an invalid security certificate."](https://www.zotero.org/support/kb/ssl_certificate_error) - dernière mise à jour de la traduction : 2024-11-14*
</div>

Le message ci-dessus ou un message similaire indique généralement que quelque chose sur votre ordinateur ou votre réseau intercepte et éventuellement surveille votre connexion à l'internet.

(Si vous obtenez une erreur de certificat pour un proxy ou une URL WebDAV, il s'agit probablement d'un [problème différent](./incomplete_cert_chain.md)).

Essayez les étapes suivantes pour déboguer le problème.

 1. Redémarrez Zotero et/ou votre ordinateur et essayez à nouveau. Cette erreur peut être causée par des problèmes de réseau intermittents.
 2. Assurez-vous que l'horloge de votre système et le fuseau horaire sont correctement réglés.
 3. Si vous obtenez la même erreur après le redémarrage, la connexion réseau de Zotero est probablement interceptée, peut-être en raison d'un serveur proxy sur votre réseau ou encore d'un logiciel de sécurité ou d'un logiciel malveillant sur votre ordinateur. Affichez les [informations du certificat](https://www.zotero.org/support/kb/site_certificate_info) pour le domaine concerné dans votre navigateur, ce qui peut vous montrer ce qui intercepte vos connexions.
   * Si votre navigateur affiche le domaine concerné comme étant validé par une entité attendue (par exemple, "Amazon" pour zotero.org), il se peut que la connexion réseau de Zotero soit configurée différemment de celle de votre navigateur.
   * Si votre navigateur affiche le domaine affecté comme étant vérifié par quelque chose d'autre et que vous reconnaissez l'entité listée (par exemple, le nom d'un logiciel de sécurité ou de votre institution), prenez les mesures qui s'imposent.
     * Si vous voyez listé un logiciel de sécurité, désactivez-le ou désactivez sa fonction d'analyse SSL/TLS/HTTPS.
     * Si votre institution figure dans la liste, votre service informatique intercepte probablement votre trafic et a installé un certificat pour une "autorité de certification personnalisée" dans votre navigateur, afin d'éviter les erreurs de sécurité résultant de l'interception. Selon la configuration de votre système, il se peut que Zotero ne fasse pas confiance à ce même certificat personnalisé par défaut, auquel cas il vous avertira correctement de l'interception de la connexion. Vous pouvez essayer de suivre les instructions de [remplacement du certificat](./cert_override.md) pour Zotero, mais sachez que votre connexion aux serveurs Zotero est surveillée par votre institution.
  * Si vous ne reconnaissez pas l'entité répertoriée :
     * Vérifiez que votre système ne contient pas de logiciels malveillants.
     * Si vous utilisez un logiciel de sécurité, essayez de le désactiver temporairement.
     * Si vous êtes dans un environnement institutionnel, demandez à votre service informatique s'il a installé une "autorité de certification personnalisée" dans votre navigateur. Si c'est le cas, vous pouvez essayer de suivre les instructions de [remplacement du certificat](./cert_override.md) pour Zotero, mais sachez que votre connexion aux serveurs Zotero est surveillée par votre institution.
     * Si vous utilisez un ordinateur portable, essayez à partir d'un autre réseau.
     * Dans de rares cas, la réinstallation de Zotero peut aider.
