# Zotero prend-il en charge les caractères non occidentaux ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Does Zotero support non-Western characters?](https://www.zotero.org/support/kb/non-western_characters) - dernière mise à jour de la traduction : 2023-03-30*
</div>

Oui, Zotero est compatible avec Unicode et peut traiter des caractères non occidentaux.