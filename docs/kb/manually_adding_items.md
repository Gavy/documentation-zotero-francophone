# Comment puis-je ajouter manuellement un document dans ma bibliothèque Zotero?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do I manually add a bibliographic item?](https://www.zotero.org/support/kb/manually_adding_items) - dernière mise à jour de la traduction : 2023-04-06*
</div>

Vous pouvez ajouter manuellement des documents en cliquant sur l'icône combinant un cercle vert et un signe plus au-dessus de la colonne du milieu (![icône nouveau document](../images/add.png)), en choisissant le type de document approprié et en complétant les informations bibliographiques dans la colonne de droite. Les nouveaux documents apparaissent dans le dossier "Ma bibliothèque" et peuvent aussi être organisés dans vos collections.

Consultez la section [Ajouter manuellement des documents](../adding_items_to_zotero.md#ajouter-manuellement-des-documents) pour plus de précisions.
