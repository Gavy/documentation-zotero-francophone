# Je suis l'éditeur/le rédacteur en chef d'une revue. Que puis-je faire pour que Zotero prenne en charge notre style ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [I'm the publisher/editor of a journal. What can I do to have Zotero support our style?](https://www.zotero.org/support/kb/requesting_styles_for_editors) - dernière mise à jour de la traduction : 2023-01-18*
</div>

C'est fantastique ! Vous êtes la personne idéale pour que votre style soit pris en charge et approuvé.

Zotero utilise le langage [Citation Style Language (CSL)](http://citationstyles.org/). En aidant à créer un style CSL, vous créez un style qui peut également être utilisé par [Mendeley](http://www.mendeley.com/), [Papers3](http://www.mekentosj.com/papers/) et de nombreuses autres applications de gestion bibliographique.

Si [le dépôt des styles Zotero](https://www.zotero.org/styles) contient déjà un style pour votre revue mais que ce style comporte des erreurs, veuillez signaler ces erreurs sur les [forums Zotero](https://www.zotero.org/forum). Sinon vous pouvez soit [créer vous-même un style CSL](https://www.zotero.org/support/dev/citation_styles), soit [demander sa création](https://github.com/citation-style-language/styles/blob/master/REQUESTING.md#requesting-csl-styles) sur les  [forums Zotero](https://www.zotero.org/forum).

Une fois que vous disposez d'un style CSL de qualité suffisante, pensez à créer un lien vers ce dernier sur votre page web dans la section "Instructions pour les auteurs", ou hébergez-le vous-même.