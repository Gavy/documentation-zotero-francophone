# Options de réinitialisation de la synchronisation Zotero

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Zotero Sync Reset Options](https://www.zotero.org/support/kb/sync_reset_options) - dernière mise à jour de la traduction : 2022-10-06*
</div>

Cette page documente les opérations spécifiques de synchronisation disponible dans l'onglet "Synchronisation -> Réinitialisation" des préférences de Zotero.

**Les opérations suivantes sont à utiliser dans de rares et spécifiques situations et ne doivent pas être utilisées pendant une utilisation normale ou un dépannage général. Dans de nombreuses situations, la réinitialisation causera des problèmes supplémentaires. Si vous n'êtes pas sûr de ce que font ces options, veuillez demander de l'aide sur le [forum Zotero](https://forums.zotero.org) avant de les utiliser.**

**Avant d'avoir recours à l'une des options présentées sur cette page, assurez-vous en premier lieu de [sauvegarder votre bibliotheque Zotero](../zotero_data.md#sauvegarder-votre-bibliotheque-zotero).**

## Remplacer la bibliothèque en ligne

"Remplacer la bibliothèque en ligne" vous permet d'écraser une bibliothèque Zotero en ligne avec les données de votre base de données Zotero locale. Cela peut être utile si vous avez apporté localement des modifications non souhaitées à une bibliothèque Zotero et que ces modifications ont déjà été synchronisées avec la bibliothèque en ligne, ou si des modifications ont été apportées sur un autre ordinateur et téléchargées vers la bibliothèque en ligne mais que ces modifications n'ont pas encore été synchronisées avec votre ordinateur actuel.

Notez que "Remplacer la bibliothèque en ligne" n'est nécessaire que si vous souhaitez annuler des modifications déjà appliquées à la bibliothèque en ligne. Ce n'est pas nécessaire si vous avez simplement effectué des changements localement et que vous voulez synchroniser ces changements. Par exemple, si la bibliothèque en ligne est vide et que vous ajoutez de nombreux documents localement, ces documents seront automatiquement téléchargés - les documents locaux ne seront pas supprimés simplement parce qu'ils n'existent pas dans la bibliothèque en ligne. De même, si vous supprimez de nombreux documents localement, ces suppressions seront automatiquement synchronisées avec la bibliothèque en ligne sans que vous n'ayez à prendre aucune mesure particulière.

### Si la bibliothèque en ligne et votre base de données locale contiennent des modifications indésirables

1. Désactivez temporairement la synchronisation automatique dans le volet "Synchronisation" des préférences de Zotero.
2. Restaurez vos données locales à partir de l'une de vos sauvegardes externes ou de l'une des sauvegardes automatiques du [répertoire de données Zotero](../zotero_data.md).
3. Utilisez "Remplacer la bibliothèque en ligne" pour télécharger cette version de votre bibliothèque. 

Voir [Restaurer votre bibliothèque Zotero à partir d'une sauvegarde](../zotero_data.md#restaurer-votre-bibliotheque-zotero-a-partir-dune-sauvegarde-et-ecraser-les-modifications-synchronisees) pour des instructions spécifiques à votre situation.

### Si la bibliothèque en ligne contient des modifications indésirables qui n'ont pas encore été synchronisées vers votre ordinateur actuel

1. Si Zotero n'est pas encore ouvert et que vous voulez l'empêcher de se synchroniser, désactivez temporairement la connexion réseau de votre ordinateur (par exemple, en désactivant le wifi), ouvrez Zotero, puis assurez-vous que la synchronisation automatique est désactivée dans le volet "Synchronisation" des préférences de Zotero.
2. Faites une sauvegarde de votre [répertoire de données Zotero](../zotero_data.md).
3. Assurez-vous que les autres ordinateurs sont entièrement synchronisés avec la bibliothèque en ligne. (Les données spécifiques synchronisées n'ont pas d'importance, car vous écraserez la bibliothèque en ligne avec la version locale, mais pour que la restauration s'applique aux autres ordinateurs sans conflits potentiels, ils doivent déjà être synchronisés. Il peut être judicieux de faire une sauvegarde du répertoire de données Zotero de chaque autre ordinateur avant d'effectuer la restauration.)
4. Choisissez "Remplacer la bibliothèque en ligne" dans le volet "Synchronisation -&gt; Réinitialisation" des préférences de Zotero. Assurez-vous de choisir la bonne bibliothèque dans le menu déroulant. Si vous devez écraser plus d'une bibliothèque en ligne, effectuez la restauration séparément pour chaque bibliothèque.

Si la restauration a réussi, vous pouvez réactiver la synchronisation automatique. Conservez une sauvegarde de votre répertoire de données Zotero jusqu'à ce que tous les ordinateurs concernés aient eu la possibilité de synchroniser la version restaurée.

## Réinitialiser l'historique de synchronisation des fichiers

Si les modifications apportées aux pièces jointes ne sont pas synchronisées (par exemple, modifications, annotations, suppression d'une pièce jointe, ajout d'une nouvelle pièce jointe), cette option réinitialisera l'historique de synchronisation entre votre base de données Zotero locale et votre service de stockage (soit les serveurs Zotero soit votre fournisseur WebDAV). Zotero comparera alors tous les fichiers joints de votre ordinateur local avec ceux de votre service de stockage, en apportant les modifications les plus récentes aux fichiers.

La réinitialisation de l'historique de synchronisation des fichiers ne devrait pas être nécessaire, donc si vous constatez que les fichiers ne se synchronisent pas correctement, consultez la section [Pourquoi ai-je le message "Le fichier joint n'a pu être trouvé" quand j'essaie d'ouvrir un fichier dans Zotero ?](./files_not_syncing.md) pour obtenir de l'aide sur le dépannage et signaler le problème.
