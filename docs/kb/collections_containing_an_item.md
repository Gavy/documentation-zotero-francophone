# Comment voir dans quelles collections se trouve mon document ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How can I see what collections my item is in?](https://www.zotero.org/support/kb/collections_containing_an_item) - dernière mise à jour de la traduction : 2024-08-28*
</div>


Zotero 7 inclut dans l'affichage détaillé d'un document (panneau de droite) une section affichant la liste des collections dans lesquelles le document est classé.

Vous pouvez également sélectionner le document puis appuyer sur la touche  “Option” sous macOS, “Ctrl” sous Windows ou “Alt” (Zotero 6)/"Ctrl" (Zotero 7) sous Linux. Toutes les collections dans lesquelles est classé le document sont alors surlignées.