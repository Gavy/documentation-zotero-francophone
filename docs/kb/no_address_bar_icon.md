# Pourquoi l'icône Zotero n'apparait-elle pas dans la barre d'adresse lorsque je consulte certaines pages web ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Why don't I see a Zotero icon in the address bar when viewing a webpage?](https://www.zotero.org/support/kb/no_address_bar_icon) - dernière mise à jour de la traduction : 2022-09-20*
</div>

Si vous avez utilisé Zotero pour Firefox auparavant et que vous ne voyez plus d'icône d'enregistrement Zotero dans la barre d'adresse de Firefox, notez que le bouton est maintenant toujours présent dans la barre d'outils de Firefox. Voir [ici](../adding_items_to_zotero.md#via-votre-navigateur-web) pour plus de détails. 