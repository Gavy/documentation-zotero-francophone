# Aperçu des différents types de documents de votre bibliothèque

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Library Item Overview](https://www.zotero.org/support/kb/library_items) - dernière mise à jour de la traduction : 2023-01-18*
</div>

Une bibliothèque Zotero est composée de "documents". Les documents peuvent prendre plusieurs formes : notices, pièces jointes et notes.

Tous les documents peuvent se voir appliquer des marqueurs et peuvent être associés à d'autres par un lien "Connexe".

## Les notices

Documents fondamentaux de la plupart des bibliothèques Zotero, les *notices* prennent la forme de références de différents types - livres, articles de revue, manuscrits - et sont composées de métadonnées bibliographiques (titre, auteur, maison d'édition, etc.). Les notices peuvent être créées manuellement en utilisant le menu déroulant "Nouveau document" ou automatiquement à partir d'un site web compatible, en cliquant sur le bouton "Enregistrer dans Zotero" dans la barre d'outils de votre navigateur.

En général, il est préférable de travailler avec des notices Zotero plutôt qu'avec des fichiers joints nus (par exemple, des PDF, des documents de traitement de texte), car les notices comportent les métadonnées bibliographiques nécessaires pour interagir avec la plupart des fonctionnalités de Zotero.

Les notices ne peuvent être que des documents de niveau supérieur. Les pièces jointes et les notes peuvent être ajoutées aux notices en tant que documents enfants.

## Les pièces jointes

Les pièces jointes sont des fichiers et des liens web sans métadonnées bibliographiques complètes. Il existe quatre types de pièces jointes.

### Liens web et autres URI

Les liens web sont essentiellement des signets vers des sites web. Lorsque vous enregistrez un lien, Zotero ne stocke que le titre de la page, l'URL et la date d'accès, et vous devez retourner sur le site pour voir le contenu de la page. Vous pouvez également joindre des liens aux URI d'autres programmes, comme les liens onenote: ou evernote:.

### Captures de page web

Les captures de page web contiennent les mêmes informations que les liens web, mais Zotero enregistre également une copie de la page telle qu'elle existe au moment de l'enregistrement, afin que vous puissiez la consulter ultérieurement même si la page web originale a changé ou disparu.

Les captures de pages web peuvent être constituées de fichiers uniques, comme des PDF, ou comporter plusieurs fichiers, comme c'est le cas d'une page HTML et de ses images associées.

### Fichiers liés

Les fichiers liés sont des liens vers des fichiers stockés en dehors du répertoire de données de Zotero sur votre ordinateur.

### Fichiers joints

Les fichiers joints sont des fichiers stockés dans le répertoire de données de Zotero. Lorsque vous importez un fichier (soit en utilisant "Joindre une copie enregistrée du fichier...", soit en faisant glisser un fichier), Zotero copie le fichier dans son répertoire de données, sans toucher à l'original. Après avoir importé un fichier, vous pouvez supprimer la copie de ce fichier externe à Zotero pour éviter toute confusion.

Chaque pièce jointe dispose d'un unique champ de note intégré.

Les pièces jointes peuvent être soit des *pièces jointes enfants* (jointes à des notices) soit des *pièces jointes indépendantes* (documents de niveau supérieur non joints à des notices). Les pièces jointes ne peuvent pas avoir de documents enfants attachés à elles.

À partir de la version 2.0b3 de Zotero, les liens web et les captures de pages web ne peuvent être créés qu'en tant que documents enfants, à l'exception temporaire des captures de PDF, qui peuvent toujours être créées en tant que documents de niveau supérieur, pour permettre l'utilisation de la fonction "Récupérer les métadonnées du PDF".


## Les notes

Les notes sont des documents textuels sans métadonnées bibliographiques complètes.

Les notes peuvent être soit des *notes filles* (attachées à des notices) soit des *notes indépendantes* (documents de niveau supérieur non attachés à des notices). Les notes ne peuvent pas avoir de documents enfants attachés à elles.

Bien que Zotero vous permette d'insérer des images dans les notes, elles ne seront pas synchronisées (et empêcheront votre bibliothèque Zotero de se synchroniser). Une meilleure prise en charge de l'insertion d'images dans les notes est prévue.
