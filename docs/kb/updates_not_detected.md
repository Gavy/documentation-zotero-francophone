# Pourquoi Zotero ne détecte-t-il pas les mises à jour?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Why isn't Zotero detecting updates?](https://www.zotero.org/support/kb/updates_not_detected) - dernière mise à jour de la traduction : 2023-03-22*
</div>

## Les mises à jour nécessitent des privilèges d'administrateur.

Zotero ne nécessite aucun privilège d'administrateur pour ses opérations et, en général, il n'en a pas besoin pour installer les mises à jour. Si on vous demande un mot de passe ou des privilèges d'administrateur lors de la mise à jour de Zotero, c'est que vous êtes dans l'une des deux situations suivantes.

1. Zotero a été installé initialement avec des privilèges d'administrateur. Cela n'est pas nécessaire. Désinstallez Zotero et réinstallez-le à partir de [zotero.org/download](https://www.zotero.org/download). Si on vous demande des privilèges d'administrateur lors du processus d'installation, cliquez sur "Annuler", plutôt que sur OK.
2. Vos administrateurs informatiques ont configuré votre système pour exiger des privilèges d'administrateur pour installer les mises à jour des programmes. Demandez à votre administrateur informatique si Zotero peut être ajouté à la liste blanche (ou liste d'inclusion) pour lui permettre de se mettre à jour.


## Aucune mise à jour automatique n'est trouvée, mais les vérifications de mises à jour manuelles fonctionnent

Si vous ne recevez pas de mises à jour automatiques pour Zotero ou une extension, mais que vous pouvez trouver des mises à jour manuellement en cliquant sur Aide → Rechercher des mises à jour… (pour Zotero) ou Outils → Extensions → Engrenage → Rechercher des mises à jour… (pour les extensions), vérifiez que les mises à jour automatiques sont activées. Pour Zotero, ouvrez l'Éditeur de configuration depuis l'[onglet Avancées](../advanced.md) des [préférences de Zotero](../preferences.md) et assurez-vous que `app.update.auto` et `app.update.enabled` sont tous deux définis sur `true`. Pour les extensions, vérifiez la mise à jour automatique des extensions dans le menu Engrenage de la fenêtre des extensions de Zotero.

## Aucune mise à jour automatique ou manuelle n'est trouvée

Si les nouvelles versions de Zotero ou des extensions ne sont pas installées automatiquement et ne sont pas détectées lorsque vous vérifiez manuellement les mises à jour, quelque chose sur votre système ou votre réseau peut intercepter les connexions sécurisées (HTTPS) vers zotero.org ou vers le serveur de mise à jour de l'extension. Pour déterminer si votre connexion est interceptée, vérifiez les [informations du certificat du site](https://www.zotero.org/support/kb/site_certificate_info).

### Correction rapide

Si les informations du certificat du site indiquent un logiciel de sécurité sur votre système (Bitdefender, Avast), désactivez la fonctionnalité d'analyse SSL/TLS/HTTPS de ce logiciel. Le nom exact de la fonctionnalité peut varier. Consultez la documentation du logiciel pour obtenir de l'aide. Pour plus de détails, continuez à lire ci-dessous.

### Plus de détails

Pour garantir la sécurité et la confidentialité de ses utilisateurs, Zotero exige que toutes les connexions soient effectuées via HTTPS, ce qui garantit que vous vous connectez directement à un site web distant et que votre connexion est chiffrée. Cependant, un logiciel installé sur votre système ou votre administrateur réseau peut remplacer les protections de sécurité de HTTPS, se faisant passer pour n'importe quel site web. Certains logiciels de sécurité font cela dans le but de fournir une sécurité supplémentaire : ils interceptent les connexions HTTPS, analysent le contenu eux-mêmes, puis ré-encryptent les données et les envoient au site web d'origine dans une nouvelle connexion. Bien que les créateurs de tels logiciels prétendent qu'ils vous protègent avec cette fonctionnalité en recherchant des logiciels malveillants servis via HTTPS, ce comportement brise une fonctionnalité de sécurité fondamentale intégrée aux navigateurs web. C'est un peu comme si quelqu'un parcourait tout votre courrier postal, lisait chaque lettre et vous prévenait s'il trouvait du courrier indésirable. Bien que ce qu'ils font soit potentiellement utile, ils n'ont vraiment pas à fouiner dans votre courrier. (Et dans certains cas, les éditeurs d'antivirus ont même [inséré leurs propres publicités et traceurs](http://www.howtogeek.com/199829/avast-antivirus-was-spying-on-you-with-adware-until-this-week/) dans les enveloppes avant de les refermer.)

Pour recevoir des mises à jour automatiques, vous avez deux options.

1. Désactivez la fonction d'analyse' SSL/TLS/HTTPS dans votre logiciel de sécurité et essayez à nouveau la mise à jour. Si les informations du certificat identifient votre institution comme étant la partie interceptante, vous devrez parler à votre administrateur réseau et demander qu'il cesse d'intercepter vos connexions sécurisées vers des sites web, bien que cela puisse être une condition d'utilisation du réseau.
2. Si vous faites confiance au logiciel ou à l'institution qui intercepte votre connexion, vous pouvez forcer Zotero à télécharger des mises à jour sur des connexions interceptées. Ouvrez l'Éditeur de configuration de Zotero depuis le [volet Avancées](../advanced.md) des [préférences de Zotero](../preferences.md). Cliquez avec le bouton droit de la souris sur la liste des paramètres qui apparaît et sélectionnez New → Boolean. Pour Zotero, saisissez `app.update.cert.requireBuiltIn` pour le nom de propriété et choisissez `true` comme valeur. Pour les extensions, saisissez `extensions.update.requireBuiltInCerts` et choisissez `true` comme valeur. Sachez qu'il n'y aura plus de garantie que vous receviez des versions légitimes de Zotero ou des extensions, à moins que vous ne retourniez à l'éditeur de configuration et que vous ne désactiviez ces préférences.

## Le connecteur Zotero pour Firefox ne détecte pas les mises à jour

Si le connecteur Zotero pour Firefox ne se met pas à jour automatiquement, vérifiez que les mises à jour automatiques sont activées depuis la fenêtre des extensions de Firefox (menu Extensions et thèmes→ Engrenage → Mettre à jour les modules automatiquement). Vérifiez également les [préférences du connecteur Zotero](../connector_preferences.md) et assurez-vous que les mises à jour automatiques y sont activées.

Si les mises à jour automatiques sont activées, essayez de mettre à jour le connecteur manuellement en cliquant sur le bouton Engrenage dans la fenêtre des extensions de Firefox et en choisissant "Vérifier les mises à jour". Si les mises à jour ne sont pas détectées manuellement, vous  rencontrer peut-être le problème d'interception de connexion sécurisée décrit ci-dessus. Pour déterminer si votre connexion est interceptée, vérifiez les [informations du certificat du site](https://www.zotero.org/support/kb/site_certificate_info).

Si les informations du certificat du site pointent vers un logiciel de sécurité sur votre système (Bitdefender, Avast), désactivez la fonction d'analyse' SSL/TLS/HTTPS de ce logiciel. Le nom exact de la fonction varie. Consultez la documentation du logiciel pour obtenir de l'aide.

Comme solution temporaire, vous pouvez installer manuellement l'extension mise à jour à partir de [zotero.org/download](https://www.zotero.org/downlod). (Normalement, Firefox empêche également les installations manuelles sur de telles connexions, mais nous avons mis en place une solution de contournement pour les autoriser.) Cependant, sans mise à jour automatique, vous pourriez rencontrer des problèmes de compatibilité ou de bogues ultérieurs qui ont déjà été corrigés.

Pour recevoir des mises à jour automatiques, vous avez deux options.

1. Désactivez la fonction d'analyse SSL/TLS/HTTPS dans le logiciel de sécurité et essayer la mise à jour à nouveau. Si les informations du certificat identifient votre institution comme étant la partie interceptant, vous devrez parler à votre administrateur réseau et demander qu'il cesse d'intercepter vos connexions sécurisées aux sites web, bien que cela puisse être une condition d'utilisation du réseau.
2. Si vous faites confiance au logiciel ou à l'institution qui intercepte votre connexion, vous pouvez forcer Firefox à télécharger les mises à jour des extensions sur des connexions interceptées. Saisissez "about:config" dans la barre d'adresse de Firefox, faites un clic droit sur la liste des paramètres qui apparaît, sélectionnez New → Boolean, saisissez `extensions.update.requireBuiltInCerts` pour le nom de la propriété et choisissez `true` comme valeur. Notez qu'il n'y aura plus de garantie que vous receviez des versions légitimes de Zotero et d'autres extensions de Firefox à moins que vous ne retourniez à about:config et que vous ne désactiviez cette préférence.
