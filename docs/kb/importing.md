# Comment puis-je importer des références dans Zotero ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do I import references into Zotero?](https://www.zotero.org/support/kb/importing) - dernière mise à jour de la traduction : 2023-04-06*
</div>

Si vous venez à Zotero depuis un autre logiciel de gestion bibliographique, consultez la page [Importer depuis d'autres logiciels de gestion bibliographique](../moving_to_zotero.md).

D'une façon générale, la meilleure méthode pour ajouter des documents dans Zotero est d'utiliser le bouton du connecteur Zotero dans votre navigateur internet. Pour plus d'informations concernant l'usage du connecteur Zotero et des autres fonctionnalités d'importation de Zotero, consultez la page [Ajouter des documents à Zotero](../adding_items_to_zotero.md).

Pour le dépannage des problèmes d'importation et d'exportation, vous pouvez essayer les étapes pertinentes de la [procédure de dépannage des problèmes d’enregistrement dans Zotero](../troubleshooting_translator_issues.md), puis adresser vos questions directement sur les [forums Zotero](https://www.zotero.org/forum).
