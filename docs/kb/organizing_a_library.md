# Comment puis-je organiser et gérer mes collections ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [What ways can I organize and manage my collections?](https://www.zotero.org/support/kb/organizing_a_library) - dernière mise à jour de la traduction : 2022-12-19*
</div>

"Ma bibliothèque" contient tous les documents de votre bibliothèque. Pour organiser ces documents, vous pouvez créer des collections et des sous-collections dans la partie gauche de la fenêtre Zotero. Notez que les documents peuvent appartenir à plusieurs collections ; il est préférable de considérer les collections comme des "listes de lecture" plutôt que comme des dossiers sur votre ordinateur. Pour ajouter une nouvelle collection, cliquez sur l'icône "Nouvelle collection..." en haut à gauche de la fenêtre Zotero ou cliquez avec le bouton droit de la souris sur le volet gauche de Zotero. Pour créer une sous-collection, faites un clic droit sur une collection et choisissez "Nouvelle sous-collection..." ou faites glisser une collection existante dans une autre collection. Pour renommer une collection, cliquez dessus avec le bouton droit de la souris (contrôle-clic sur Mac) et sélectionnez "Renommer la collection...". Pour ajouter des documents à une collection, faites-les glisser depuis le volet central.

Vous pouvez également organiser votre bibliothèque à l'aide de marqueurs. Cliquez sur un document dans le volet central. Sélectionnez ensuite l'onglet "Marqueurs" dans le volet de droite. Cliquez sur "Ajouter", tapez le nom du marqueur, puis appuyez sur Entrée/Retour. Pour ajouter plusieurs marqueurs à la fois, tapez Maj+Entrée/Retour. Saisissez chaque marqueur sur une nouvelle ligne en tapant Entrée/Retour entre chacun d'eux, et enfin tapez à nouveau Maj+Entrée/Retour pour les ajouter tous en même temps. Pour filtrer votre bibliothèque ou une collection sur un marqueur, sélectionnez le marqueur dans le panneau du sélecteur de marqueurs en bas à gauche de la fenêtre Zotero. Vous pouvez également faire glisser des documents du panneau central vers un marqueur dans le sélecteur de marqueurs, pour ajouter ce marqueur à tous les documents sélectionnés.

Voir [Collections et marqueurs](../collections_and_tags.md) pour plus d'informations.
