# Comment citer une source secondaire dans Zotero ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [How do you cite a secondary source in Zotero?](https://www.zotero.org/support/kb/secondary_citation) - dernière mise à jour de la traduction : 2023-02-09*
</div>

On peut parfois souhaiter citer une source secondaire. Par exemple,

 1. British Foreign Office, FO371, FO Minute, 30 mars 1943, A3068/4/2. Vol 22507, cité dans Bryce Wood, Dismantling the Good Neighbor Policy, (Austin : University of Texas Press, 1985), 18
2. (Johnson, 1982, cité dans Smith, 2004)

Dans ce cas, il faut citer la source qui a été effectivement consultée pour la citation (dans les exemples ci-dessus, Wood [1985] et Smith [2004]). Entrez le contenu restant dans [le champ "Préfixe" de l’extension de traitement de texte de Zotero](../word_processor_plugin_usage.md#prefixe-et-suffixe). Par exemple,

1. British Foreign Office, FO371, FO Minute, 30 mars 1943, A3068/4/2. Vol 22507, cité dans
2. Johnson, 1982, cité dans

De cette façon, les sources originales qui n'ont pas été consultées n'apparaissent pas dans la bibliographie, ce qui est généralement considéré comme une bonne pratique.
