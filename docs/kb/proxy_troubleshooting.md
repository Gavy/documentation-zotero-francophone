# Pourquoi ne puis-je pas accéder à un site proxyfié lorsque le connecteur Zotero est activé ?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Why can't I access a proxied site when the Zotero Connector is enabled?](https://www.zotero.org/support/kb/proxy_troubleshooting) - dernière mise à jour de la traduction : 2025-02-09*
</div>

Lorsque vous essayez d'accéder à un site auquel vous avez déjà accédé via un serveur proxy, le connecteur Zotero peut [vous rediriger automatiquement via votre proxy](../connector_preferences.md#onglet-proxies).

Si un site est inaccessible via votre navigateur lorsque le connecteur Zotero est activé, mais fonctionne dans d'autres navigateurs ou lorsque le connecteur est désactivé, il se peut qu'il y ait un problème avec un paramètre de proxy que le connecteur a stocké.

Pour éviter que cela ne se reproduise, veuillez suivre les étapes suivantes.

1. Générez un "[Debug ID](../debug_output.md)" du connecteur pour une tentative de chargement de la page.
2. Ouvrez le volet "Proxies" des préférences du connecteur Zotero et recherchez une entrée de proxy pertinente. Copiez le "Hostname" et le "URL Scheme", puis cliquez sur l'entrée et copiez les paramètres de la section ci-dessous.

Postez le "Debug ID" et les détails du proxy dans un nouveau fil de discussion sur le forum afin que l'équipe de développement puisse enquêter.

Vous pouvez alors supprimer l'entrée de proxy du volet "Proxies", ce qui devrait résoudre le problème temporairement.