# Pourquoi Zotero ne  trouve-t-il pas un fichier joint?

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Why can't Zotero find a stored file?](https://www.zotero.org/support/kb/missing_stored_file) - dernière mise à jour de la traduction : 2023-01-20*
</div>

Plusieurs raisons peuvent expliquer qu'un [fichier joint](../attaching_files.md#fichiers-joints) ne puisse pas être trouvé.

1. Vous avez créé le fichier sur un autre ordinateur et il n'a pas encore été synchronisé avec cet ordinateur. Voir [la page concernant les fichiers non synchronisés](./files_not_syncing.md).
2. Le fichier individuel a été déplacé ou supprimé en dehors de Zotero. Vous pouvez utiliser le bouton "Localisation en cours..." dans la boîte de dialogue "Fichier introuvable" pour retrouver et sélectionner le fichier. Lorsque vous utilisez "Localisation en cours..." , si vous sélectionnez un fichier qui a été renommé dans le répertoire de stockage d'origine, Zotero mettra à jour la base de données pour qu'elle pointe vers ce nouveau fichier. Si vous sélectionnez un fichier à un autre emplacement, ce fichier sera copié dans le répertoire de stockage correct.
3. Vous avez déplacé ou supprimé le répertoire "storage" du [répertoire de données de Zotero](../zotero_data.md).
4. Pour une raison ou pour une autre, vous utilisez finalement un répertoire de données Zotero différent de celui que vous utilisiez précédemment sur cet ordinateur. Soit vous visualisez une base de données différente, soit vous avez synchronisé les données - mais pas les fichiers, qui ne sont peut-être pas disponibles en ligne - depuis la bibliothèque en ligne. Dans ce cas, il est préférable de [localiser le bon répertoire de données Zotero](../zotero_data.md#localiser-des-donnees-zotero-manquantes) sur cet ordinateur.

Notez que Zotero ne supprime jamais les fichiers d'un disque lorsque les pièces jointes correspondantes existent toujours dans la base de données. Par conséquent, si vos fichiers sont manquants, soit ils n'ont pas été synchronisés, soit quelque chose s'est produit en dehors de Zotero.
