# Zotero pour iOS

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [iOS](https://www.zotero.org/support/ios) - dernière mise à jour de la traduction : 2023-12-09*
</div>

[Zotero pour iOS](https://apps.apple.com/us/app/zotero/id1513554812) est la meilleure solution pour travailler avec votre bibliothèque Zotero sur iPad ou iPhone.

![capture d'écran](images/zotero-for-ios-interface.png)

Zotero pour iOS vous permet de travailler avec vos données Zotero où que vous soyez.

* Synchronisez votre bibliothèque personnelle et vos bibliothèques de groupe.
* Affichez et modifiez les détails des documents.
* Organisez les documents en collections.
* Générez des citations et des bibliographies dans l'un des 10'000 styles de citation (APA, Chicago, MLA, etc.).
* Prenez des notes sur vos recherches.
* Lisez des PDF, surlignez et ajoutez des notes, des images et des annotations.
* Enregistrez facilement des documents et des PDF du web vers Zotero via le bouton "Partager" de Safari ou d'autres applications (autres navigateurs, Twitter, etc.).
* Ajoutez rapidement des livres et des articles à votre bibliothèque Zotero en scannant les codes-barres des livres ou les DOI des articles à l'aide de l'appareil photo de votre iPhone ou iPad.
* Évitez de vous fatiguer les yeux grâce à la prise en charge complète du mode sombre, y compris lors de la lecture de PDF.

De retour sur votre ordinateur, vous pouvez ajouter les annotations de PDF saisies sur votre iPad ou iPhone à vos notes Zotero et insérer ces notes dans votre [document de traitement de texte](https://zotero.hypotheses.org/4145#section1-3) avec des citations Zotero actives, ou exporter ces notes en Markdown.

N'hésitez pas à publier sur les [Forums Zotero](https://www.zotero.org/forum) tout rapport de bug ou toute demande de fonctionnalité. Veillez à mentionner "iOS", "iPad" ou "iPhone" dans le titre de votre message.

[Télécharger Zotero pour iOS sur l'App Store](https://apps.apple.com/us/app/zotero/id1513554812)
