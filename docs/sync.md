# Synchronisation

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Syncing](https://www.zotero.org/support/sync) - dernière mise à jour de la traduction : 2024-12-10*
</div>

Tandis que Zotero stocke par défaut toutes les données en local sur votre ordinateur, la fonctionnalité de synchronisation de Zotero vous permet d'accéder à votre bibliothèque Zotero sur n'importe quel ordinateur disposant d'un accès Internet. La synchronisation Zotero se compose de deux parties : la synchronisation des données et la synchronisation des fichiers.

## Synchronisation des données

La synchronisation des données fusionne les documents de la bibliothèque, les notes, les liens, les marqueurs, etc. - tout sauf les fichiers joints - entre votre ordinateur local et les serveurs Zotero, ce qui vous permet de travailler avec vos données depuis n'importe quel ordinateur sur lequel Zotero est installé. La synchronisation des données vous permet également de consulter votre bibliothèque en ligne sur zotero.org. Elle est gratuite, n'a pas de limite de stockage et peut être utilisée sans la synchronisation de fichiers.

La première étape pour synchroniser votre bibliothèque Zotero est de [créer un compte Zotero](https://www.zotero.org/user/register/) ; ce compte est aussi utilisé pour les forums Zotero. Une fois votre compte créé, ouvrez l’onglet "Synchronisation" des [préférences](./preferences.md) de Zotero et entrez vos informations de connexion dans la rubrique "Synchronisation des données".

![Fenêtre de paramétrage de la synchronisation : saisie du nom d'utilisateur et du mot de passe du compte Zotero à associer à l'instance locale](./images/preferences_sync_empty_FR.png)

Par défaut, Zotero synchronise vos données avec le serveur à chaque modification. Pour désactiver la synchronisation automatique, décochez "Synchroniser automatiquement" dans cette rubrique.

![Présente l’ensemble des paramètres de synchronisation, une fois le compte utilisateur associé](./images/preferences_sync_settings_user_FR.png)

En plus de la synchronisation automatique, vous pouvez synchroniser manuellement à tout moment en cliquant sur le bouton "Synchroniser avec zotero.org" sur le côté droit de la barre d'outils Zotero.

![Gestion manuelle de la synchronisation avec le bouton "Synchroniser avec zotero.org](./images/sync_FR.png)

Lors de la synchronisation, Zotero applique automatiquement les modifications dans les deux sens - toute modification effectuée à un endroit sera appliquée à chaque ordinateur synchronisé. Si un document a été modifié à plusieurs endroits entre les synchronisations, vous verrez s'afficher une boîte de dialogue de résolution de conflit vous demandant quelle version vous souhaitez conserver. Si vous avez un nouvel ordinateur, vous pouvez simplement configurer la synchronisation et Zotero téléchargera automatiquement toutes les données de votre bibliothèque en ligne.

## Synchronisation des fichiers

La synchronisation des données synchronise les documents de la bibliothèque, mais ne synchronise pas les fichiers joints (PDF, fichiers audio et vidéo, images, etc.). Pour synchroniser ces fichiers, vous pouvez [paramétrer la synchronisation des fichiers](./preferences_sync.md) pour accompagner la synchronisation des données, en utilisant soit le stockage de Zotero soit WebDAV.

### Le stockage de Zotero

Le stockage de Zotero est l'option de synchronisation des fichiers recommandée. Elle présente plusieurs avantages par rapport à la synchronisation WebDAV, tels que la synchronisation des fichiers pour les [bibliothèques de groupes](./groups.md), l'accès Web aux PDF et autres pièces jointes, une configuration plus facile, une compatibilité garantie et une performance de téléchargement améliorée pour certains fichiers. Chaque utilisateur de Zotero se voit attribuer 300 Mo d'espace de stockage gratuit pour les fichiers joints, avec des [offres de stockage étendues](https://www.zotero.org/support/storage#storage_pricing) disponibles à l'achat.

Reportez-vous à la documentation [Zotero Storage](https://www.zotero.org/storage) pour plus d'informations.

### WebDAV

WebDAV est un protocole stanbardisé pour transférer des fichiers sur le web, et il peut être utilisé pour synchroniser les fichiers de votre bibliothèque personnelle. Les bibliothèques de groupe ne peuvent en revanche pas utiliser WebDAV. Votre employeur ou votre institution de recherche peut peut-être vous fournir un stockage WebDAV. Il existe également de nombreux services tiers qui prennent en charge WebDAV, gratuitement ou non - voir la page [Liste des fournisseurs WebDAV connus pour fonctionner avec Zotero](./kb/webdav_services.md) pour une liste de services compatibles.

Une fois que vous disposez des informations de votre compte WebDAV, renseignez l’URL fournie par le service, votre nom d’utilisateur et votre mot de passe dans la rubrique "Synchronisation des fichiers" de l’onglet "Synchronisation" des [préférences](./preferences_sync.md) de Zotero. Assurez-vous de sélectionner la valeur 'http' ou 'https' appropriée - si vous n'êtes pas sûr, essayez d'abord 'https'. Après avoir entré les informations, cliquez sur "Tester la connexion au serveur". Si Zotero vérifie avec succès le compte WebDAV, vous êtes prêt à utiliser la synchronisation de fichiers via WebDAV.

![Montre où paramétrer la synchronisation des fichiers joints en utilisant un stockage WebDAV](./images/preferences_sync_webdav_FR.png)

La synchronisation des fichiers devrait fonctionner correctement avec n'importe quel serveur WebDAV. Les développeurs de Zotero ne peuvent pas fournir de support pour les serveurs WebDAV tiers.

### Fichiers joints liés

Vous pouvez également utiliser la synchronisation des fichiers Zotero sur plusieurs ordinateurs en utilisant [des fichiers liés](./attaching_files.md#fichiers-joints-et-fichiers-lies). Plutôt que de joindre aux documents Zotero des copies enregistrées de fichiers, vous pouvez joindre des liens à des fichiers stockés dans un répertoire de synchronisation en ligne, tel que Dropbox ou Google Drive. Lorsque vous utilisez des fichiers joints liés, vous devez configurer la fonction [Répertoire de base pour les pièces jointes liées](./advanced.md#repertoire-de-base-pour-les-pieces-jointes-liees) de Zotero. Ce paramétrage permet à Zotero de retrouver vos fichiers sur chaque ordinateur, même si le chemin vers le répertoire de synchronisation en ligne est différent. L’extension [ZotFile](https://www.zotero.org/support/plugins#attachment_file_management) peut faciliter la gestion des pièces jointes liées en déplaçant automatiquement les fichiers joints dans un répertoire déterminé lorsque vous les importez.

Voir [Solutions alternatives de synchronisation](#solutions-alternatives-de-synchronisation) pour plus de détails.

## La synchronisation en pratique

Si Zotero est paramétré de façon à synchroniser automatiquement, les modifications seront synchronisées quasiment immédiatement. Autrement, vous devez lancer une synchronisation manuelle en cliquant sur le bouton de synchronisation à la droite de la barre d’outils Zotero.

Si vous renseignez vos informations de compte sur plusieurs ordinateurs (onglet "Synchronisation" des préférences de Zotero), Zotero va tout synchroniser de façon transparente. Votre attention ne sera requise que si un document ou un fichier est modifié depuis deux endroits différents avant que Zotero ne puisse les synchroniser. Dans ce cas, une fenêtre de résolution de conflit s’affichera afin que vous puissiez déterminer les changements à conserver.

Si vous synchronisez depuis un seul ordinateur, vous pouvez toujours consulter votre bibliothèque en ligne sur zotero.org depuis n'importe quel ordinateur. Si quelque chose arrive à votre ordinateur ou si vous voulez commencer à utiliser Zotero sur un autre ordinateur, configurez simplement vos informations de compte sur le nouvel ordinateur. Zotero récupèrera l’intégralité de votre bibliothèque depuis le serveur.

## Solutions alternatives de synchronisation

Si pour une raison quelconque vous ne pouvez pas utiliser les fonctions de synchronisation de Zotero, il existe d'autres moyens de synchroniser vos données. Toutefois il peut y avoir des risques et des limitations importants en fonction de l'approche que vous choisissez.

**Stocker le répertoire de données Zotero directement dans un répertoire de stockage en nuage est [extrêmement susceptible de corrompre votre base de données Zotero](./kb/data_directory_in_cloud_storage_folder.md) et ne doit pas être fait.**

Si vous voulez éviter toute synchronisation de données sur les serveurs Zotero :

* Vous pouvez fermer Zotero, copier manuellement votre [répertoire de données Zotero](./zotero_data.md) entier dans un répertoire synchronisé sur un des ordinateurs, puis le restaurer - toujours avec Zotero fermé - sur un autre ordinateur, comme si vous effectuiez une [sauvegarde et une restauration](./zotero_data.md#restaurer-votre-bibliotheque-zotero-a-partir-dune-sauvegarde) de vos données.

Si vous souhaitez utiliser la synchronisation des données Zotero mais utiliser un service externe pour synchroniser uniquement vos fichiers joints dans Zotero :

1.  Vous pouvez utiliser des [fichiers liés](./attaching_files.md#fichiers-joints-et-fichiers-lies) plutôt que des copies enregistrées des fichiers, en ne conservant que les fichiers joints dans le dossier synchronisé de façon externe à Zotero.

