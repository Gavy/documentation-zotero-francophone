# Comment visualiser dans sa bibliothèque Zotero les documents cités dans un fichier de traitement de texte?

!!! danger "Contenu produit par le collectif Traduction-Zotero-fr"
    **Cette page a été entièrement écrite par le collectif Traduction-Zotero-fr et ne fait pas partie de la documentation officielle Zotero - dernière mise à jour : 2024-12-12**
    { data-search-exclude }

La fonctionnalité première du service en ligne [Reference Extractor](http://rintze.zelle.me/ref-extractor/) est, comme son nom l’indique, d’extraire des références bibliographiques d’un document rédigé, à partir d'un fichier de traitement de texte. Reverence Extractor fait ainsi partie des [solutions pour importer des bibliographies insérées dans des documents Word](../kb/importing_formatted_bibliographies.md). Pour rappel, cette extraction peut être réalisée si les conditions suivantes sont réunies.

* Le fichier de traitement de texte est un fichier Word au format **.docx** ou un fichier LibreOffice au format **.odt**.
* Les citations ont été insérées avec **Zotero** ou **Mendeley**. Pour un fichier LibreOffice, les citations doivent être stockées sous la forme de **Signets** et non de **Marques de référence** (option par défaut).
* Les citations sont toujours actives et n’ont pas été converties en texte.

C'est une fonctionnalité complémentaire de Reference Extractor qui nous intéresse.

À partir du fichier de traitement de texte téléchargé dans l’interface en ligne, Reference Extractor offre en effet à l'étape 2 du processus de sélectionner les documents correspondants dans la bibliothèque Zotero, avec l'option _Select in Zotero_. Il ne reste plus alors qu’à attribuer à ces documents un marqueur ou une collection pour les identifier durablement dans la bibliothèque.

![Interface de Reference Extractor, avec mise en valeur de l'option de sélection des documents dans la  bibliothèque](../images/kbfr_refextractor.png)
