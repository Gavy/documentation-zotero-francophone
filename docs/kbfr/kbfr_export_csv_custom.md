# Créer un format d'export CSV personnalisé

<div data-search-exclude markdown>
!!! danger "Contenu produit par le collectif Traduction-Zotero-fr"
    **Cette page a été entièrement écrite par le collectif Traduction-Zotero-fr et ne fait pas partie de la documentation officielle Zotero - dernière mise à jour : 2024-11-22**
</div>

De même qu'il prend en charge de nombreux formats d'import, Zotero propose de multiples format pour [l'export des données de votre bibliothèque Zotero](../kb/exporting.md). Outre les formats bibliographiques, il propose un export CSV. Notez que l'export CSV permet d'exporter uniquement les données bibliographiques et les notes. Si vous souhaitez exporter également les fichiers joints, il conviendra de privilégier un autre format d'export tel que RIS, Zotero RDF ou encore BibTex.

![Formats d'export avec CSV mis en évidence](../images/kbfr_CSV_exportformats.png)

Dans l’export CSV par défaut, tous les champs des références bibliographiques sont exportés, selon un ordre déterminé. Il est possible de personnaliser tant les champs exportés que leur ordre, en modifiant le paramétrage du convertisseur que Zotero utilise pour cet export.

Suivez les étapes ci-dessous pour créer un format d'export CSV personnalisé.

1. Trouvez le convertisseur CSV dans le dossier `translators` de votre [répertoire de données Zotero](../zotero_data.md). Il s'agit du fichier nommé `CSV.js`. Vous pouvez afficher votre répertoire de données Zotero depuis les _Paramètres_ > _Avancé_ > _Fichiers et dossiers_ > _Ouvrir le répertoire de données_.

![Mise en évidence du fichier CSV.js enregistré dans le dossier translators du répertoire de données Zotero.](../images/kbfr_CSV_translator.png)

2. Fermez Zotero, créez une copie du fichier `CSV.js` et renommez ce nouveau fichier pour le distinguer du fichier original. Prenons comme exemple `CSV_personnalise.js`.

3. Ouvrez le fichier `CSV_personnalise.js` dans un éditeur de texte comme Notepad++ ; il s'agit d'Atom dans les copies d’écran ci-dessous.

4. Pour distinguer votre convertisseur personnalisé du convertisseur CSV standard et évitez qu'il ne soit écrasé par d'éventuelles mises à jour de ce dernier, effectuez les deux modifications suivantes.

 * En haut du code, modifiez la valeur de l’identifiant du convertisseur, `translatorID`.
 * Changez également l’intitulé, `label`, au profit de l’intitulé que vous souhaitez voir apparaître dans le menu déroulant de choix du format d’export ; prenons _CSV personnalisé_.

![Modification de l’identifiant et du libellé du nouveau convertisseur](../images/kbfr_CSV_IDlabel.png)

5. Faites défiler le code jusqu’à la section `var exportedFields = [` et commentez, supprimez et/ou réordonnez les champs que vous ne souhaitez pas exporter : vous paramétrez ici l'ordre des colonnes de votre fichier CSV. Assurez-vous de ne pas vous tromper dans les virgules, il doit y en avoir une pour séparer chaque colonne. Dans l’exemple ci-dessous, on a simplement exclu les dates d’ajout et de modification en les commentant, c’est-à-dire en insérant deux barres obliques `//` avant la partie de code à commenter.

![Exemple de modification du convertisseur CSV](../images/kbfr_CSV_codecommente.png)

6. Enregistrez votre fichier, redémarrez Zotero et testez votre nouvel export!
