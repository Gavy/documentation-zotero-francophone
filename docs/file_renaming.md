# Renommage des fichiers

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [File Renaming](https://www.zotero.org/support/file_renaming) - dernière mise à jour de la traduction : 2025-01-23*
</div>

Zotero renomme automatiquement les PDF et autres fichiers enregistrés dans votre bibliothèque en fonction des informations bibliographiques de leur document parent (titre, auteur, etc.), ce qui vous évite d'avoir à trier des piles de fichiers aux noms aléatoires ou à renommer manuellement chaque nouveau fichier selon le format de votre choix.

## Comportement de renommage

Zotero renomme toujours les fichiers enregistrés depuis le web, via le connecteur Zotero ou les fonctionnalités "Ajouter un document par son identifiant" et "Trouver un PDF disponible".

Par défaut, Zotero renomme également les [fichiers joints](./attaching_files.md#fichiers-joints-et-fichiers-lies) PDF et EPUB que vous ajoutez aux documents en tant que première pièce jointe enfant, ainsi que les fichiers pour lesquels il réussit à [récupérer les métadonnées](./retrieve_pdf_metadata.md). Vous pouvez désactiver cette fonction en décochant l'option "Renommer automatiquement les pièces jointes en utilisant les métadonnées parentes" dans le volet "Générales" des paramètres de Zotero. Si un document a déjà une pièce jointe, les pièces jointes additionnelles ne sont pas renommées automatiquement, afin d'éviter de modifier les noms de fichiers des fichiers supplémentaires.

Les fichiers liés ne sont pas automatiquement renommés, mais vous pouvez activer l'option "Renommer les fichiers liés" dans le volet "Générales" des paramètres de Zotero pour que le renommage s'applique à eux également.

Vous pouvez ajuster quels types de fichiers Zotero renomme automatiquement via la [préférence cachée](https://www.zotero.org/support/preferences/hidden_preferences) `extensions.zotero.autoRenameFiles.fileTypes`.

## Titre de pièce jointe vs. nom de fichier

Voir [Pourquoi les pièces jointes portent-elles des noms tels que "PDF" ou "Version acceptée" dans la liste des documents, au lieu de leur nom de fichier ?](./kb/attachment_title_vs_filename.md)

## Personnaliser le format des noms de fichiers

Par défaut, Zotero nomme les fichiers d'après le créateur (1-2 auteurs ou éditeurs), l'année et le titre du document parent :

```
Lee et al - 2023 - The First Room-Temperature Ambient-Pressure Superconductor.pdf
```

Si Zotero a toujours renommé les fichiers automatiquement, Zotero 7 introduit une nouvelle syntaxe puissante pour personnaliser les noms de fichiers. Le format par défaut peut être personnalisé à partir du panneau "Générales" des paramètres de Zotero.

Voici la chaîne de caractères du modèle par défaut :

```
{{ firstCreator suffix=" - " }}{{ year suffix=" - " }}{{ title truncate="100" }}
```

Les variables et paramètres suivants sont pris en charge.

### Variables

| Variable  | Description |
| :--------------- |:---------------|
| `authors` |   Principaux créateurs du document parent ; en fonction du type de document, il peut s'agir d'auteurs ou d'artistes mais non d'éditeurs ou autres contributeurs. |  
| `editors`  |  Editeurs du document parent.   |  
| `creators`  |  Tous les créateurs du document parent.   |  
| `firstCreator`  |  Créateur du document parent  (1-2 auteurs ou éditeurs), identique à la valeur de la colonne "Créateur".  |  
| `itemType`  |  Type du document parent. Consultez la [liste complète des types de document reconnus (page en anglais)](https://api.zotero.org/itemTypes)  |  
| `attachmentTitle`  |  Titre de la pièce jointe qui est renommée ou créée.   | 
| `year`  |  Année, extraite du champ date du document parent.   |  
| N'importe quel champ du document  |  La liste complète des champs est disponible en bas de cette page.   |  

Si la valeur d'une variable commence ou se termine par une espace, ce qui est susceptible de se produire lorsqu'elle est utilisée avec le paramètre `truncate`, ces espaces sont supprimées du nom de fichier.

### Paramètres

| Paramètre  | Variables | Valeur par défaut  | Description |
| :--------------- |:---------------|:--------------- |:---------------|
| `start`  | Toutes |   | Tronque la valeur de la variable à partir du début. Par exemple, `{{ title start="5" }}` est remplacé par la valeur du titre du document parent, en omettant les 5 premiers caractères. Ce paramètre peut être combiné avec `truncate`. |
| `truncate`  | Toutes |   | Tronque la valeur de la variable à un nombre fixe de caractères : par exemple `{{ title truncate="20" }}` est remplacé par les 20 premiers caractères du titre du document parent. La troncature intervient après l'application de tous les autres paramètres, à l'exception de `prefix`, `suffix` et `case`. |
| `prefix`  | Toutes |   | Préfixe la variable avec le(s) caractère(s) donné(s) : par exemple `{{ title prefix="titre" }}` est remplacé par le mot "titre" suivi du titre du document parent. Si la variable est vide (par exemple, le titre du document parent est vide), l'ensemble de l'expression, y compris le préfixe, est ignoré. |
| `suffix`  | Toutes |   | Ajoute à la fin de la variable le(s) caractère(s) donné(s) : par exemple `{{ title suffix="!" }}` est remplacé par le titre du document parent suivi d'un point d'exclamation. Si la variable est vide (par exemple, le titre du document parent est vide), l'ensemble de l'expression, y compris le suffixe, est ignoré. |
| `case`  | Toutes |   | Convertit la casse d'une variable ; les valeurs suivantes sont acceptées : `upper`, `lower`, `sentence`, `title`, `hyphen`, `snake`, `camel`. Par exemple, `{{ title case="snake" }}` donnera `titre_rendu_comme_ceci` dans le nom du fichier. |
| `replaceFrom`  | Toutes |   |  Utilise une expression rationnelle pour remplacer une chaîne de caractères correspondante dans la variable par la valeur spécifiée par le paramètre `replaceTo`. Par exemple, `{{ title replaceFrom="problème" replaceTo="solution" }}` est remplacé par le titre du document parent où la première occurrence du mot "problème" est remplacée par le mot "solution". Le paramètre `regexOpts` permet une configuration plus poussée.  |
| `replaceTo`  | Toutes |   |  Définit la valeur de remplacement à utiliser lors de la correspondance avec une expression rationnelle (voir `replaceFrom`. Il est possible de spécifier des groupes de capture définis dans `replaceFrom`. Par exemple, pour préfixer toutes les occurrences des mots "chien" et "chat" par le mot "super-" dans le titre d'un document, le modèle suivant peut être utilisé : `{{ title replaceFrom="(chien|chat)" replaceTo="super-$1" regexOpts="gi" }}`.|
| `regexOpts`  | Toutes |  'i'  |  Définit les [caractères spéciaux](https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Regular_expressions) à utiliser lors de la correspondance  avec des expressions rationnelles (voir `replaceFrom`). Par exemple, `{{ title replaceFrom="\s+" regexOpts="g" }}` est remplacé par le titre du document parent avec tous les espaces blancs supprimés (sans `regexOpts`, seul le premier espace blanc serait supprimé).  |
| `match`  | Toutes |   |  Utilise une expression rationnelle pour tester la présence d'une chaîne de caractères correspondante dans la variable. Ce paramètre est utile dans les conditions et ne peut être utilisé avec aucun autre paramètre, à l'exception des `regexOpts`. Par exemple, le modèle suivant renverra l'URL du document parent parent uniquement si le nom de domaine de l'URL est zotero.org : `{{ if {{ url match="^https?://zotero.org.*?$" }} }}{{ url }}{ endif }}`.   |
| `max`  | `authors`, `editors`, `creators` |   |  Limite le nombre de créateurs à utiliser : par exemple `{{ editors max="1" }}` est remplacé par le premier éditeur du document parent. Cela peut être configuré avec le paramètre `regexOpts`.|
| `name`  | `authors`, `editors`, `creators` | `family`  | Personnalise la façon dont le nom du créateur apparaît dans le nom du fichier, avec l'une des options suivantes. `family-given` utilise le nom complet du créateur, en commençant par son nom de famille ; `given-family` utilise également le nom complet mais inverse l'ordre ; et les options `given` et `family` n'utilisent qu'une partie du nom du créateur du document parent. |
| `name-part-separator`  | `authors`, `editors`, `creators` | ` ` (espace simple unique)  | Définit les caractères à utiliser pour séparer le prénom et le nom de famille ; il est particulièrement utile lorsqu'il est combiné avec `initialize`. |
| `join`  | `authors`, `editors`, `creators` |   `,` | Définit les caractères à utiliser pour séparer les créateurs consécutifs. |
| `initialize`  | `authors`, `editors`, `creators` |   | Permet l'utilisation d'initiales pour une partie ou la totalité du nom des créateurs, avec l'une des options suivantes. `full` utilise les initiales pour la totalité du nom ; `given` et `family` n'utilisent les initiales que pour la partie du nom qu'ils désignent respectivement. L'ordre des parties du nom est contrôlé par le paramètre `name` et seules les parties incluses dans le paramètre `name` peuvent être converties en initiales. Par exemple, `{{ authors name="given-family" initialize="given" }` est remplacé par une liste d'auteurs séparés par des virgules, où le prénom de chaque auteur est remplacé par une initiale, suivie d'un point et d'une espace (par exemple J. Smith, D. Jones). |
| `initialize-with`  | `authors`, `editors`, `creators` |  `.` | Contrôle le caractère ajouté à l'initiale, si la partie du nom a été réduite à l'initiale. |
| `localize`  | `itemType` |   | Permet d'utiliser ou non la valeur localisée de la variable pour le type de document : par exemple `{{ itemType localize="true" }}` est remplacé par le type du document parent orthographié dans la langue utilisée par Zotero. |

### Exemples

Une année de publication, suivie d'une liste d'auteurs séparés par des tirets, suivie d'un titre tronqué à 30 caractères :

Modèle : `{{ year suffix="-" }}{{ authors name="family-given" initialize="given" initialize="-" join="-" suffix="-" case="hyphen" }}{ title truncate="30" case="hyphen" }}`

Nom de fichier : `2023-lee-sukbae-kim-ji-hoon-kwon-young-wan-la-première-chambre-température-amb.pdf`

Tout ce qui n'est pas inclus dans une accolade `{{` est copié littéralement dans le nom de fichier :

Modèle : `{{ itemType localize="true" }} de {{ year }} par {{ authors max="1" name="given-family" initialize="given" }}`

Nom de fichier : `Preprint de 2023 par S. Lee.pdf`

Les modèles prennent également en charge les conditions, certaines parties du modèle peuvent être incluses ou exclues en utilisant une combinaison de `if`, `elseif`, `else`. La condition doit se terminer par `endif`. Le modèle ci-dessous utilise le DOI pour les articles de revue et les prépublications, l'ISBN pour les livres et le titre pour tout autre type de document.

```
{{ if itemType == "book" }}
{{ISBN}}
{{ elseif itemType == "preprint" }}
{{ DOI }}
{{ elseif itemType == "journalArticle" }}
{{ DOI }}
{{ else }}
{{ title }}
{{ endif }}
```
Il est possible d'utiliser des expressions rationnelles pour faire correspondre des valeurs et modifier le comportement du modèle. Par exemple, le modèle suivant préserve les noms de pièces jointes courants (tels que « Full Text »), mais pour les pièces jointes dont les titres ne correspondent pas, il utilise le modèle de nom de fichier standard de Zotero.

```
{{ if {{ attachmentTitle match="^(full.*|submitted.*|accepted.*)$" }} }}
{{ attachmentTitle }}
{{ else }}
{{ firstCreator suffix=" - " }}{{ year suffix=" - " }}{{ title truncate="100" }}
{{ endif }}
```
### Liste complète des champs

* `abstractNote`
* `accessDate`
* `applicationNumber`
* `archive`
* `archiveID`
* `archiveLocation`
* `artworkMedium`
* `artworkSize`
* `assignee`
* `audioFileType`
* `audioRecordingFormat`
* `billNumber`
* `blogTitle`
* `bookTitle`
* `callNumber`
* `caseName`
* `citationKey`
* `code`
* `codeNumber`
* `codePages`
* `codeVolume`
* `committee`
* `company`
* `conferenceName`
* `country`
* `court`
* `date`
* `dateDecided`
* `dateEnacted`
* `dictionaryTitle`
* `distributor`
* `docketNumber`
* `documentNumber`
* `DOI`
* `edition`
* `encyclopediaTitle`
* `episodeNumber`
* `extra`
* `filingDate`
* `firstPage`
* `format`
* `forumTitle`
* `genre`
* `history`
* `identifier`
* `institution`
* `interviewMedium`
* `ISBN`
* `ISSN`
* `issue`
* `issueDate`
* `issuingAuthority`
* `journalAbbreviation`
* `label`
* `language`
* `legalStatus`
* `legislativeBody`
* `letterType`
* `libraryCatalog`
* `manuscriptType`
* `mapType`
* `meetingName`
* `nameOfAct`
* `network`
* `numPages`
* `number`
* `numberOfVolumes`
* `organization`
* `pages`
* `patentNumber`
* `place`
* `postType`
* `presentationType`
* `priorityNumbers`
* `proceedingsTitle`
* `programTitle`
* `programmingLanguage`
* `publicLawNumber`
* `publicationTitle`
* `publisher`
* `references`
* `reportNumber`
* `reportType`
* `reporter`
* `reporterVolume`
* `repository`
* `repositoryLocation`
* `rights`
* `runningTime`
* `scale`
* `section`
* `series`
* `seriesNumber`
* `seriesText`
* `seriesTitle`
* `session`
* `shortTitle`
* `status`
* `studio`
* `subject`
* `system`
* `thesisType`
* `title`
* `type`
* `university`
* `url`
* `versionNumber`
* `videoRecordingFormat`
* `volume`
* `websiteTitle`
* `websiteType`
