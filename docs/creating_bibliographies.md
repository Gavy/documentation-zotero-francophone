# Créer des bibliographies

<div data-search-exclude markdown>
!!! info "Informations sur la page"
    *Consulter cette page dans la documentation officielle de Zotero : [Creating Bibliographies](https://www.zotero.org/support/creating_bibliographies) - dernière mise à jour de la traduction : 2024-11-21*
</div>

## Les extensions de traitement de texte

Vous utilisez Microsoft Word, LibreOffice ou Google Docs? [Les extensions de traitement de texte de Zotero](./word_processor_integration.md) vous permettent d'ajouter des citations et des bibliographies directement à partir de vos documents.

## Copie rapide

![Aperçu d'une bibliographie dans un fichier au format .rtf](./images/rtf-bib_FR.png)

Si vous souhaitez simplement ajouter rapidement des références à un article, un courriel ou un billet de blog, la copie rapide de Zotero est la façon la plus simple de procéder. Sélectionnez simplement les documents dans le panneau central et faites-les glisser dans n'importe quel champ de texte. Zotero crée automatiquement une bibliographie mise en forme pour vous. Pour copier des citations au lieu de références, maintenez la touche Maj enfoncée au début du glisser-déposer.

Pour paramétrer vos paramètres de copie rapide, ouvrez les [paramètres](./preferences.md) de Zotero et sélectionnez [Exportation](./export.md). Depuis cet onglet vous pouvez :

-   définir le format d'export par défaut,
-   définir des paramètres spécifiques à un site,
-   choisir si vous voulez que Zotero inclue des balises HTML lors de la copie.

Vous pouvez également utiliser l'option "Edition" → "Copier la bibliographie" ou le raccourci clavier Ctrl/Cmd+Maj+C pour copier des références dans le presse-papiers et les coller ensuite dans des documents. Pour copier des citations plutôt que des références, utilisez "Edition" → "Copier la citation" ou le raccourci clavier Ctrl/Cmd+Maj+A.

Outre la sortie bibliographique, la copie rapide prend aussi en charge des formats d'export comme BibTeX et RIS.

## Menu contextuel pour créer une citation/bibliographie

Pour créer une bibliographie ou une liste de citations dans Zotero, mettez en surbrillance une ou plusieurs références, puis cliquez avec le bouton droit de la souris (ou contrôle-cliquez sur Mac) pour sélectionner "Créer une bibliographie à partir du (des) document(s)...". Sélectionnez ensuite un style bibliographique pour la mise en forme de votre citation/bibliographie et choisissez soit de créer une liste de *Citations/Notes*, soit une *Bibliographie*. Choisissez ensuite l'une des quatre méthodes de création suivantes pour créer votre citation/bibliographie.

-   *Enregistrer au format RTF* vous permet d'enregistrer votre bibliographie en tant que fichier au format de texte enrichi.
-   *Enregistrer au format HTML* vous permet d'enregistrer la bibliographie en tant que fichier HTML pour l'afficher dans un navigateur web. Ce format intègre également des métadonnées permettant aux autres utilisateurs de Zotero affichant le document d'en capturer les informations bibliographiques.
-   *Copier dans le presse-papiers* vous permet d'enregistrer la bibliographie dans votre presse-papiers pour la coller dans n'importe quel champ texte.
-   *Imprimer* enveoie votre bibliographie directement à une imprimante .

## Analyse RTF

Avec l'[analyse RTF](./rtf_scan.md), vous pouvez écrire en texte simple et utiliser Zotero pour finaliser vos citations et bibliographies dans le style de votre choix.
